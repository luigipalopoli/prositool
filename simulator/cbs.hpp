/*!
 * @file    cbs.hpp
 * 
 * @brief   This class defines a header for the Constant Bandwidth Server
 *          scheduler.
 * 
 * @author  Luigi Palopoli           <luigi.palopoli@unitn.it>
 *          Bernardo Villalba Frías  <b.r.villalba.frias@hva.nl>
 * 
 * @version 2.0
 * 
 * @date    30 November 2019
 * 
 * This program is a free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation.
 */
#ifndef CBS_HPP
#define CBS_HPP

#include "../utils/auxiliary_functions.hpp"
#include "../utils/help_messages.hpp"
#include "../utils/prosit_types.hpp"

namespace PrositCore {

  class Cbs {

    private:

      uint64_t Ts;                ///< Reservation period.
      uint64_t Qs;                ///< Reservation budget.
      uint64_t residual_time;     ///< Residual time for the k-th job.
      int64_t start_time;         ///< Start time of the k-th job.

      std::vector<results_t> results;     ///< Probability of respecting the deadline.
      std::vector<times_t> times;         ///< Timing details of the jobs.

    public:

      /// @brief Constructor.
      ///
      /// This is the constructor for the Constant Bandwidth Server.
      Cbs(uint64_t Tsd,
          uint64_t Qsd, 
          int64_t start_timed) : 
          Ts(Tsd),
          Qs(Qsd),
          residual_time(Qsd),
          start_time(start_timed) { }

      /// @brief Destructor.
      ///
      /// This is the destructor for the Constant Bandwidth Server.
      ~Cbs() { }

      /// @brief Compute the finishing time of the job quantised to Ts.
      ///
      /// 
      /// .
      ///
      /// @param .
      /// @param .
      /// @param .
      int64_t computeFinishTime(int64_t &c_time);

      /// @brief Compute the values for the analysis of the next job.
      ///
      /// 
      /// .
      ///
      /// @param .
      /// @param .
      /// @param .
      void computeNextData(int64_t f_time, int64_t a_time, int64_t c_time, int64_t i_time);

      void insertTiming(times_t tmp_times);
      void insertResults(results_t tmp_results);

      std::vector<times_t> const& getJobsTiming() const {
        return times;
      }

      void saveResults(const std::string &file_results, const std::string &file_times);

  };

}

#endif
