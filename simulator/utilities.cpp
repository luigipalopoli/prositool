/*!
 * @file    cbs.cpp
 * 
 * @brief   Simulator of a Resource Reservation
 *
 * @author  Luigi Palopoli          <luigi.palopoli@unitn.it>
 *          Bernardo Villalba Frias <b.r.villalba.frias@hva.nl>
 * 
 * @version 3.0
 * 
 * @date    30 November 2019
 * 
 * This program is a free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation.
 */
#include "utilities.hpp"

using namespace std;

/*! @brief Determine the range of the given distribution */
//void obtainRange(const string &filename, uint64_t &cmin, uint64_t &cmax) {

  /* Variables for the files */
  //ifstream myfile;
  //string line;
  //stringstream sstr;

  /* Counter of elements within each line */
  //int size;

  /* Variables for the data */
  //long double index, value;
  
  /* Open the data file */
  //myfile.open(filename.c_str());
  //if (myfile.fail()) {
    //EXC_PRINT_2("ERROR: Unable to find the file: ", filename);
  //}

  /* Check if the file was properly open */
  //if (!myfile.is_open()) {
    //EXC_PRINT_2("ERROR: Unable to open the file: ", filename);
  //}

  /* Iterate over the file */
  //while (myfile.good()) {
 
    /* Obtain a line of the file */
    //getline(myfile, line);
 
    /* Initialize the elements counter */
    //size = 0;

    /* Check whether the lines are over */
    //if (line.find_first_not_of(' ') == std::string::npos) {
      //break;
    //}

    /* Obtain the line */
    //sstr.clear();
    //sstr << line;

    /* Iterate over the line */
    //while (!sstr.eof()) {

      /* Check whether the element is numeric */
      //if (!(sstr >> value)) {
        //EXC_PRINT_2("ERROR: Incorrect format for file: ", filename);
      //}

      /* Update the number of elements within the line*/
      //size++;

    //}

    /* Check the correct number of elements within each line */
    //if (size > 2) {
      //EXC_PRINT_2("ERROR: Unknown format for file: ", filename);
    //}

    /* Re-obtain the line */
    //sstr.clear();
    //sstr << line;

    /* Parse each element of the line */
    //sstr >> index >> value;

    /* Check if the PMF has negative values */
    //if (value < 0) {
      //EXC_PRINT_2("ERROR: There are negative values in the file: ", filename);
    //}

    /* Determine the minimum index */
    //if (index < cmin) {
      //cmin = index;
    //}

    /* Determine the maximum index */
    //if (index > cmax) {
      //cmax = index;
    //}

  //}

  /* Close the file */
  //myfile.close();

//}

/*! @brief Load the given distribution */
//void loadDistribution(const string &filename, double eps, uint64_t delta, bool growing, vector<cdf> &cdf_comp_times) {

  /* Variables for the distributions ranges */
  //uint64_t cmin = numeric_limits<uint64_t>::max();
  //uint64_t cmax = numeric_limits<uint64_t>::min();

  /* Check the PMF of the computation time */
  //obtainRange(filename, cmin, cmax);

  /* Variable for the PMF of the computation time */
  //pmf *tmp_prob_mass = new pmf(cmin, cmax, 1, eps);

  /* Load the PMF of the computation time */
  //tmp_prob_mass->load(filename);

  /* Resample the PMF */
  //pmf prob_mass = tmp_prob_mass->resample(delta, growing);

  /* Variable for the CDF of the computation time */
  //cdf cum_dist(prob_mass.getMin(), prob_mass.getMax(), delta, eps);

  /* Obtain the CDF of the computation times */
  //pmf2cdf(prob_mass, cum_dist);

  /* Store the CDF of the computation times in the vector */
  //cdf_comp_times.push_back(cum_dist);
    
  //delete tmp_prob_mass;

//}

/*! @brief Load the computation times */
void loadTimes(string data_file, vector<int64_t> &times) {

  /* Variable declaration */
  ifstream data;
  int64_t comp_time;
  
  /* Open the data file */
  data.open(data_file);
  if (data.fail()) {
    cerr << "ERROR: The file was not found" << endl;
  }

  /* Iterate over the file */
  while (data >> comp_time) {

    /* Fill the vector */
    times.push_back(comp_time);

  }

}

/*! @brief Obtain the transition matrix */
void obtainTransitionMatrix(const std::string &filename, int &modes, Eigen::MatrixXd &cdf_trans_mat) {

  /* Variable declaration */
  ifstream data;
  Eigen::MatrixXd transition_matrix(modes, modes);
  
  /* Open the data file */
  data.open(filename.c_str());
  if (data.fail()) {
    EXC_PRINT_2("ERROR: Unable to find the file: ", filename);
  }

  /* Iterate over the rows of the file */
  for (int i = 0; i < modes; i++) {

    /* Iterate over the columns of the file */
    for (int j = 0; j < modes; j++) {

      /* Fill the matrix */
      data >> transition_matrix(i, j);

    }

  }

  /* Obtain the first column */
  cdf_trans_mat.block(0, 0, modes, 1) = transition_matrix.block(0, 0, modes, 1);

  /* Iterate over the columns of the transition matrix */
  for (int i = 1; i < modes; i++) {

    /* Accumulate the probability */
    cdf_trans_mat.block(0, i, modes, 1) = cdf_trans_mat.block(0, i - 1, modes, 1) + transition_matrix.block(0, i, modes, 1);

  }

}

/*! @brief Generate a random computation time based on a given CDF */
int64_t obtainRandomNumber(const cdf &cum) {

  /* Variable declaration */
  random_device rd;
  mt19937 gen(rd());
  uniform_real_distribution<double> dis(0.0, 1.0);

  int64_t comp_time = 0;

  /* Obtain a random probability */
  double prob = dis(gen);

  /* Iterate over the CDF */
  for (uint64_t i = 0; i < cum.getSize(); i++) {

    /* Verify if the generated probability has been reached */
    if (cum.get(i) > prob) {

      /* Obtain the computation time from the CDF */
      comp_time = (int64_t(cum.getMin()) + int64_t(i)) * int64_t(cum.getGranularity());

      if (comp_time < 0)
        cout << "ERROR1: " << comp_time << endl;

      break;

    }

  }

  if (comp_time == 0) {
    comp_time = int64_t(cum.getMax()) * int64_t(cum.getGranularity());
  }

  return comp_time;

}

/*! @brief Convert a PMF into a CDF */
void pmf2cdf(const pmf &prob, cdf &cum) {

  /* Initialize the variables */
  double sum = 0.0;

  /* Iterate over the PMF */
  for (uint64_t i = 0; i < prob.getSize(); i++) {

    /* Accumulate the probability */
    sum += prob.get(i);

    /* Set the value */
    cum.set(i, sum);

  }

  /* Complete the CDF to 1 */
  if (cum.get(cum.getMax() - cum.getMin()) < (1.0 - cum.epsilon)) {
    cerr << "WARNING: The resulting CDF did not sum to 1." << endl;
    cum.set(cum.getMax() - cum.getMin(), 1.0);
  }

  /* Check possible errors with the CDF */
  if (cum.check() != cdf::ERR_CODES::CDF_OK)
    EXC_PRINT("ERROR: The resulting CDF was ill formed.");

  return;

}

/*! @brief Convert a CDF into a PMF */
void cdf2pmf(const cdf &cum, pmf &prob) {

  /* Initialize the variables */
  double prev = 0.0;

  /* Iterate over the CDF */
  for (uint64_t i = 0; i < cum.getSize(); i++) {

    /* Set the value */
    prob.set(i, cum.get(i) - prev);

    /* Update the previous value */
    prev = cum.get(i);

  }

  return;

}

/*! @brief Store the relevant results */
void storeResults(string file_name, const vector<results_t> &results) {

  /* Variable for the log files */
  ofstream results_file;

  /* Open the log file */
  results_file.open(file_name);

  /* Iterate over the vector of results */
  for (size_t i = 0; i < results.size(); i++) {

    /* Prints out the current element */
    results_file << results[i].deadline << " " //<< results[i].num_hits << " " 
        //<< results[i].num_jobs << " " 
        << setprecision(15) << (double)results[i].num_hits / (double)results[i].num_jobs 
        << endl;

  }

  /* Close the log file */
  results_file.close();

}

/*! @brief Store the starting and finishing times */
void storeTimes(string file_name, const vector<times_t> &times) {

  /* Variable for the log files */
  ofstream results_file;

  /* Open the log file */
  results_file.open(file_name);

  /* Iterate over the vector of times */
  for (size_t i = 0; i < times.size(); i++) {

    /* Prints out the current element */
    results_file << times[i].execution << /*" " << times[i].start << " " 
        << times[i].end << " " << times[i].response <<*/ endl;

  }

  /* Close the log file */
  results_file.close();

}

/*! @brief Compute the finishing time of the job quantised to Ts */
int64_t finish_time(const input_parameters_t &params, int64_t &c_time,
    int64_t s_time, uint64_t r_time) {

  int64_t f_time = 0;

  /* Substract the residual time available for the job */
  c_time -= r_time;

  /* The residual time is enough to finish the job */
  if (c_time < 0) {

    /* Finishing time equal to one reservation period */
    f_time = s_time + params.Ts;

  }
  else {

    /* Finishing time equal to the required reservation periods */
    f_time = s_time + params.Ts + ((c_time + params.Qs - 1) / params.Qs) * params.Ts;

  }

  return f_time;

}

/*! @brief Compute the values for the analysis of the next job */
void next_data(const input_parameters_t &params, int64_t f_time, int64_t a_time,
    int64_t c_time, int64_t i_time, uint64_t &r_time, int64_t &s_time) {

  /* The job finishes before the deadline */
  if (f_time <= a_time + i_time) {

    /* The next residual time is equal to the budget */
    r_time = params.Qs;

    /* The next start time is equal to the periodic arrival time */
    s_time = a_time + i_time;

  }
  else {

    /* The next residual time is equal to the remaining time */
    r_time = params.Qs - (c_time % params.Qs);

    /* The residual time determine the next start time */
    if (r_time == params.Qs) {

      /* The next start time is equal to the finishing time */
      s_time = f_time;

    }
    else {

      /* The next start time is the last reservation period */
      s_time = f_time - params.Ts;

    }

  }

  return;

}
