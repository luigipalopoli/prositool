/*!
 * @file    cbs.cpp
 * 
 * @brief   This class defines an implementation for the Constant Bandwidth
 *          Server scheduler.
 * 
 * @author  Luigi Palopoli           <luigi.palopoli@unitn.it>
 *          Bernardo Villalba Frías  <b.r.villalba.frias@hva.nl>
 * 
 * @version 2.0
 * 
 * @date    30 November 2019
 * 
 * This program is a free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation.
 */
#include "cbs.hpp"

using namespace std;

namespace PrositCore {

  void Cbs::insertTiming(times_t tmp_times) {

    times.push_back(tmp_times);

  }

  void Cbs::insertResults(results_t tmp_results) {

    results.push_back(tmp_results);

  }

  void Cbs::saveResults(const string &file_results, const string &file_times) {

    /* Variable for the log files */
    ofstream times_file, results_file;

    /* Open the log file */
    times_file.open(file_times);

    /* Iterate over the vector of times */
    for (size_t i = 0; i < times.size(); i++) {

      /* Prints out the current element */
      times_file << times[i].execution << " " << times[i].start << " " 
                 << times[i].end << " " << times[i].response << endl;

    }

    /* Close the log file */
    times_file.close();

    /* Open the log file */
    results_file.open(file_results);

    /* Iterate over the vector of results */
    for (size_t i = 0; i < results.size(); i++) {

      /* Prints out the current element */
      results_file << fixed << results[i].deadline << " " //<< results[i].num_hits << " " 
                 //<< results[i].num_jobs << " " 
                   << setprecision(20) << (double)results[i].num_hits / (double)results[i].num_jobs 
                   << endl;

    }

    /* Close the log file */
    results_file.close();

  }

  /*! @brief Compute the finishing time of the job quantised to Ts */
  int64_t Cbs::computeFinishTime(int64_t &c_time) {

    int64_t f_time = 0;

    /* Substract the residual time available for the job */
    c_time -= residual_time;

    /* The residual time is enough to finish the job */
    if (c_time < 0) {

      /* Finishing time equal to one reservation period */
      f_time = start_time + Ts;

    }
    else {

      /* Finishing time equal to the required reservation periods */
      f_time = start_time + Ts + ((c_time + Qs - 1) / Qs) * Ts;

    }

    return f_time;

  }

  /*! @brief Compute the values for the analysis of the next job */
  void Cbs::computeNextData(int64_t f_time, int64_t a_time, int64_t c_time, int64_t i_time) {

    /* The job finishes before the deadline */
    if (f_time <= a_time + i_time) {

      /* The next residual time is equal to the budget */
      residual_time = Qs;

      /* The next start time is equal to the periodic arrival time */
      start_time = a_time + i_time;

    }
    else {

      /* The next residual time is equal to the remaining time */
      residual_time = Qs - (c_time % Qs);

      /* The residual time determine the next start time */
      if (residual_time == Qs) {

        /* The next start time is equal to the finishing time */
        start_time = f_time;

      }
      else {

        /* The next start time is the last reservation period */
        start_time = f_time - Ts;

      }

    }

    return;

  }

}

