/*!
 * @file    cbs.cpp
 * 
 * @brief   Simulator of a Resource Reservation
 *
 * @author  Luigi Palopoli          <luigi.palopoli@unitn.it>
 *          Bernardo Villalba Frias <b.r.villalba.frias@hva.nl>
 * 
 * @version 3.0
 * 
 * @date    30 November 2019
 * 
 * This program is a free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation.
 */
#ifndef UTILITIES_HPP
#define UTILITIES_HPP

#include <vector>
#include <fstream>
#include <sstream>
#include <random>

#include "exceptions.hpp"
#include "pmf.hpp"
#include "cdf.hpp"

/*! @brief Structure for the results */
struct results_t {

  int deadline;     /*!< Relative deadline to be respected */
  int num_hits;     /*!< Number of jobs that respect the deadline */
  int num_jobs;     /*!< Total number of jobs */

  results_t() : deadline(0), num_hits(0), num_jobs(0) { }

};

/*! @brief Structure for the response times */
struct times_t {

  int64_t execution;    /*!< Execution time of the job */
  uint64_t start;        /*!< Starting time of the job */
  uint64_t end;          /*!< Finishing time of the job */
  uint64_t response;     /*!< Response time of the job */

  times_t() : execution(0), start(0), end(0), response(0) { }

};

/*! @brief Structure for the input parameters */
struct input_parameters_t {

  uint64_t Ts;    /*!< Reservation period */
  uint64_t Qs;    /*!< Budget */

  input_parameters_t() : Ts(0), Qs(0) { }

};

/// @brief Return the maximum and minimum indices of a distribution.
///
/// This function is used to determine the minimum and maximum indices
/// of a distribution in order to create the proper vector.
///
/// @param filename is the file containing the distribution.
/// @param cmin is the minimum index of the distribution.
/// @param cmax is the maximum index of the distribution.
void obtainRange(const std::string &filename, uint64_t &cmin, uint64_t &cmax);

/// @brief Load the given distribution.
///
/// This function loads the PMF of a distribution, performs a resample of 
/// such distribution and finally, converts it into a CDF to be used in 
/// the number generation.
///
/// @param filename is the file containing the distribution.
/// @param eps is the epsilon error for the computations.
/// @param delta is the granularity for the resampling process.
/// @param growing is the direction of the approximation.
/// @param cdf_comp_times is the vector with the CDF of the distribution.
void loadDistribution(const std::string &filename, double eps, uint64_t delta, bool growing, std::vector<cdf> &cdf_comp_times);

/*! @brief Load the computation times */
///
/// This function loads the computation times from a file.
///
/// @param data_file is the file containing the computation times.
/// @param times is the vector with the computation times.
void loadTimes(std::string data_file, std::vector<int64_t> &times);

/*! @brief Obtain the transition matrix */
///
/// This function initially loads the probability transition matrix of the 
/// Markov chain. Once the transition matrix is loaded, it is transformed 
/// into a CDF transition matrix with respect to the rows. This matrix
/// is used to determine, based on an uniformly distributed random number,
/// the next mode (represented by the columns) for the system.
///
/// @param filename is the file containing the transition matrix.
/// @param num_modes is the number of modes.
/// @param cdf_trans_mat is the cdf of the transition matrix.
void obtainTransitionMatrix(const std::string &filename, int &num_modes, Eigen::MatrixXd &cdf_trans_mat);

/*! @brief Generate a random computation time */
///
/// This function generates a random computation time based on a given CDF.
///
/// @param cum is the CDF to obtain the numbers.
int64_t obtainRandomNumber(const cdf &cum);

/*! @brief Generate a random computation time */
///
/// This function selects a random computation time from a set of experimentally
/// obtained computation times .
///
/// @param times is the set of computation times to take the numbers from.
int64_t obtainRandomNumber(const std::vector<int64_t> &times);

/*! @brief Convert a PMF into a CDF */
///
/// This function obtains the cumulative distribution function from a given
/// probability mass function.
///
/// @param prob is the PMF.
/// @param cum is the CDF.
void pmf2cdf(const pmf &prob, cdf &cum);

/*! @brief Convert a CDF into a PMF */
///
/// This function obtains the probability mass function from a given
/// cumulative distribution function.
///
/// @param cum is the CDF.
/// @param prob is the PMF.
void cdf2pmf(const cdf &cum, pmf &prob);

/*! @brief Store the relevant results */
///
/// This function stores into a file the number of jobs that have respected 
/// the deadline.
///
/// @param file_name is the file where the data will be saved.
/// @param results is the vector with the results.
void storeResults(std::string file_name, const std::vector<results_t> &results);

/*! @brief Store the starting and finishing times */
///
/// This function stores into a file the response time of every job.
///
/// @param file_name is the file where the data will be saved.
/// @param results is the vector with the times.
void storeTimes(std::string file_name, const std::vector<times_t> &times);

/*! @brief Compute the finishing time of the job quantised to Ts */
int64_t finish_time(const input_parameters_t &params, int64_t &c_time,
    int64_t s_time, uint64_t r_time);

/*! @brief Compute the values for the analysis of the next job */
void next_data(const input_parameters_t &params, int64_t f_time, int64_t a_time,
    int64_t c_time, int64_t i_time, uint64_t &r_time, int64_t &s_time);

#endif
