/*!
 * @file    cbs_simulator.cpp
 * 
 * @brief   A CLI program for the simulator of a CBS scheduler.
 * 
 * @author  Luigi Palopoli           <luigi.palopoli@unitn.it>
 *          Bernardo Villalba Frías  <b.r.villalba.frias@hva.nl>
 * 
 * @version 2.0
 * 
 * @date    30 November 2019
 * 
 * This program is a free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation.
 */
#include <getopt.h>

#include "cbs.hpp"

using namespace PrositCore;
using namespace PrositAux;
using namespace Eigen;
using namespace std;

static uint64_t T = 0;
static uint64_t Ts = 0;
static uint64_t Qs = 0;
static uint64_t granularity = 1;
static uint64_t num_modes = 1;
static double eps = 1e-4;
static string path_comp_time;
static string path_inter_time;
static string path_trans_mat;
static string path_results = "simulated_deadline_hit_probabilities.txt";
static string path_times = "simulated_comp_times.txt";
static int64_t curr_mode = 1;
static int64_t round_dec = 8;
static uint64_t max_deadline = 0;
static int64_t max_len = 100000;
static bool is_pmf = false;
static bool is_periodic = true;

/* Parse the command line arguments */
static int opts_parse(int argc, char *argv[]) {

  int opt;

  /* Short description for the arguments */
  static const char short_options[] = "T:t:q:g:d:m:l:e:C:Z:M:c:o:r:R:ph";

  /* Long description for the arguments */
  static struct option long_options[] = {

    {"is_pmf",           required_argument, 0, 'p'},
    {"task_period",      required_argument, 0, 'T'},
    {"server_period",    required_argument, 0, 't'},
    {"budget",           required_argument, 0, 'q'},
    {"granularity",      required_argument, 0, 'g'},
    {"max_deadline",     required_argument, 0, 'd'},
    {"num_modes",        required_argument, 0, 'm'},
    {"sim_length",       required_argument, 0, 'l'},
    {"current_mode",     required_argument, 0, 'c'},
    {"round_decimal",    required_argument, 0, 'o'},
    {"epsilon",          required_argument, 0, 'e'},
    {"comp_time_path",   required_argument, 0, 'C'},
    {"inter_time_path",  required_argument, 0, 'Z'},
    {"trans_mat_path",   required_argument, 0, 'M'},
    {"times_path",       required_argument, 0, 'r'},
    {"results_path",     required_argument, 0, 'R'},
    {"help",             no_argument,       0, 'h'},
    {0, 0, 0, 0},

  };

  /* Iterate over the list of the arguments */
  while ((opt = getopt_long(argc, argv, short_options, long_options, 0)) != -1) {

    switch (opt) {

      /* Task period */
      case 'T':
        T = atoi(optarg);
        break;

      /* Reservation period */
      case 't':
        Ts = atoi(optarg);
        break;

      /* Budget */
      case 'q':
        Qs = atoi(optarg);
        break;

      /* Granularity */
      case 'g':
        granularity = atoi(optarg);
        break;

      /* Maximum deadline */
      case 'd':
        max_deadline = atoi(optarg);
        break;

      /* Maximum deadline */
      case 'l':
        max_len = atoi(optarg);
        break;

      /* Number of MCTM modes */
      case 'm':
        num_modes = atoi(optarg);
        break;

      /* Current mode of the Markov chain */
      case 'c':
        curr_mode = atoi(optarg);
        break;

      /* Number of decimals to round up */
      case 'o':
        round_dec = atoi(optarg);
        break;

      /* Path to the computation times PMFs */
      case 'C':
        path_comp_time = optarg;
        break;

      /* Path to the interarrival times PMF */
      case 'Z':
        path_inter_time = optarg;
        break;

      /* Path to the probability transition matrix */
      case 'M':
        path_trans_mat = optarg;
        break;

      /* Path to the transition matrix */
      case 'r':
        path_times = optarg;
        break;

      /* Path to the emission matrix */
      case 'R':
        path_results = optarg;
        break;

      /* Epsilon error */
      case 'e':
        eps = atof(optarg);
        break;

      /* Analitic flag */
      case 'p':
        is_pmf = true;
        break;

      /* Print the help */
      case 'h':
        help_simul();
        break;

      default:
        EXC_PRINT("ERROR: Incorrect parameters during the parsing.");

    }

  }

  return optind;

}

/*! @brief Simulator of a Resource Reservation */
int main(int argc, char *argv[]) {

  /* Variables for the time analysis */
  long long t_start = 0, t_solution_start = 0, t_end = 0;

  /* Variables for the distributions ranges */
  uint64_t zmin = numeric_limits<uint64_t>::max();
  uint64_t zmax = numeric_limits<uint64_t>::min();

  try {

    /* Obtain the initial computation time */
    t_start = my_get_time();

    /* Parse the command line arguments */
    opts_parse(argc, argv);

    /* Check the scheduling parameters */
    if ((Ts == 0) || (Qs == 0)) {
      EXC_PRINT("ERROR: The scheduling parameters have not been set.");
    }

    /* Check the maximum deadline to analyse */
    if (max_deadline <= 0) {
      EXC_PRINT("ERROR: The maximum deadline has not been properly set.");
    }

    /* Vector with the computation times */
    vector<uint64_t> observations;

    /* Variables for the distributions ranges */
    uint64_t cmin = numeric_limits<uint64_t>::max();
    uint64_t cmax = numeric_limits<uint64_t>::min();

    /* Variable for the transition matrix */
    MatrixXd cdf_transition_matrix(num_modes, num_modes);

    vector<cdf> cdf_comp_times;

    if (is_pmf) {

      /* Load the transition matrix */
      if (num_modes == 1) {
        cdf_transition_matrix(0, 0) = 1;
      }
      else {

        /* The probability transition matrix is passed as argument */
        if (!path_trans_mat.empty()) {

          MatrixXd transition_matrix = MatrixXd::Zero(num_modes, num_modes);

          /* Load the transition matrix */
          loadMatrix(path_trans_mat, transition_matrix);

          /* Obtain the first column */
          cdf_transition_matrix.block(0, 0, num_modes, 1) = transition_matrix.block(0, 0, num_modes, 1);

          /* Iterate over the columns of the transition matrix */
          for (uint64_t i = 1; i < num_modes; i++) {

            /* Accumulate the probability */
            cdf_transition_matrix.block(0, i, num_modes, 1) = cdf_transition_matrix.block(0, i - 1, num_modes, 1) + transition_matrix.block(0, i, num_modes, 1);

          }

        }
        else {
          EXC_PRINT("ERROR: The probability transition matrix is missing.");
        }

      }

      /* Iterate over the number of modes */
      for (uint64_t i = 1; i <= num_modes; i++) {

        /* Variables for the distributions ranges */
        cmin = numeric_limits<uint64_t>::max();
        cmax = numeric_limits<uint64_t>::min();

        /* Variable for the name of the computation times */
        char comp_time_file[250];

        /* Build the string with the name */
        sprintf(comp_time_file, "%s%lu.txt", path_comp_time.c_str(), i);

        /* Check the PMF of the computation time */
        obtainRange(comp_time_file, cmin, cmax);

        /* Variable for the PMF of the computation time */
        pmf tmp_prob_mass(cmin, cmax, eps);

        /* Load the PMF of the computation time */
        tmp_prob_mass.load(comp_time_file);

        /* Resample the PMF */
        unique_ptr<pmf> prob_mass = move(tmp_prob_mass.resample(granularity, true));

        /* Variable for the CDF of the computation time */
        cdf cum_dist(prob_mass->getMin(), prob_mass->getMax(), eps);

        /* Obtain the CDF of the computation times */
        pmf2cdf(*prob_mass, cum_dist);

        /* Store the CDF of the computation times in the vector */
        cdf_comp_times.push_back(cum_dist);

      }

    }
    else {

      /* Load the observation samples */
      if (!path_comp_time.empty()) {
        loadObservations(path_comp_time, observations, cmin, cmax, granularity);
      }
      else {
        EXC_PRINT("ERROR: The file with the observations is missing.");
      }

    }

    /* The PMF for the interarrival time is passed as argument */
    if (!path_inter_time.empty()) {

      /* Check the PMF of the interarrival time */
      obtainRange(path_inter_time, zmin, zmax);

    }
    else {

      /* The task is periodic */
      if (T != 0) {

        /* Minimum and maximum indices equal to the task period */
        zmin = T;
        zmax = T;

      }
      else {
        EXC_PRINT("ERROR: The distribution of the interarrival time is missing.");
      }

    }

    /* Variable for the PMF of the interarrival time */
    unique_ptr<pmf> u(new pmf(zmin, zmax, eps));

    /* The PMF for the interarrival time is passed as argument */
    if (!path_inter_time.empty()) {

      /* Load the PMF of the interarrival time */
      u->load(path_inter_time);

    }
    else {

      /* Set the PMF of the interarrival time */
      u->set(0, 1.0);
      T = zmin;

    }

    /* TODO: Resample the interarrival times */
    /* Resample the PMF of the interarrival time */
    //unique_ptr<pmf> prob_mass = move(u->resample(Ts, false));

    /* TODO: Convert to CDF the interarrival times */
    /* Variable for the CDF of the computation time */
    //cdf cum_dist(prob_mass->getMin(), prob_mass->getMax(), granularity, eps);

    /* Obtain the CDF of the computation times */
    //pmf2cdf(*prob_mass, cum_dist);

    /* Vector to contain the computation and interarrival times */
    //vector<cdf> cdf_inter_times;

    /* Obtain the final parsing time */
    t_solution_start = my_get_time();

    /* Variables for the time management */
    int64_t arrival_time = 0;       /*!< Arrival time of the k-th job */
    int64_t acum_arrival_time = 0;  /*!< Sum of the arrival times */
    int64_t start_time = 0;         /*!< Start time of the k-th job */
    int64_t finishing_time = 0;     /*!< Finishing time of the k-th job */
    int64_t comp_time;
    int64_t interarrival_time, act_arrival_time;
    int64_t num_hit = 0;
    uint64_t D = 0;

    /* TODO: Check the validity of this code */
    //if (!is_periodic) {

      /* Obtain the actual arrival time of the first job */
      //act_arrival_time = 0;//obtainRandomNumber(cdf_inter_times[0]);

      /* Accumulate the actual arrival time */
      //acum_arrival_time += act_arrival_time;

      /* Quantise the arrival time to the previous Ts */
      //arrival_time = ((acum_arrival_time / Ts) * Ts);

      /* Obtain the start time of the first job */
      //start_time = arrival_time;

    //}

    /* Create the Constant Bandwidth Server */
    Cbs cbs(Ts, Qs, start_time);

    /* Variables for the random numbers */
    double switch_probability = 0.0;
    random_device rd;
    mt19937 gen(rd());
    uniform_real_distribution<double> dis(0.0, 1.0);

    /* Iterate for the defined number of iterations */
    for (uint64_t i = 0; i < (uint64_t)max_len; i++) {

      comp_time = 0;

      if (is_pmf) {

        /* Obtain the probability of switching states */
        switch_probability = round(dis(gen) * pow(10.0, round_dec)) / pow(10.0, round_dec);

        /* Iterate over the modes */
        for (uint64_t j = 0; j < num_modes; j++) {

          /* Identify the next state */
          if (switch_probability <= cdf_transition_matrix(curr_mode - 1, j)) {

            /* Update the current mode */
            curr_mode = j + 1;

            /* Obtain the computation time */
            comp_time = 0;//obtainRandomNumber(cdf_comp_times[curr_mode - 1]);

            if (comp_time <= 0)
              cout << "ERROR2: " << comp_time << endl;

            break;

          }

        }

      }
      else {

        comp_time = observations[i % observations.size()];

	    }

      /* Periodic arrival time */
      if (is_periodic) {

        arrival_time = i * T;

      }

      if (comp_time <= 0)
        cout << "ERROR3: " << comp_time << endl;

      /* Create the structure with the times */
      times_t tmp_times;
      tmp_times.execution = comp_time;

      if (comp_time <= 0)
        cout << "ERROR4: " << comp_time << endl;

      /* Obtain the finishing time of the job */
      finishing_time = cbs.computeFinishTime(comp_time);

      tmp_times.start = arrival_time;
      tmp_times.end = finishing_time;
      tmp_times.response = finishing_time - arrival_time;

      /* Store the relevant times */
      cbs.insertTiming(tmp_times);

      if (!is_periodic) {

        /* Obtain the next actual arrival time */
        act_arrival_time = 0;//obtainRandomNumber(cdf_inter_times[0]);

        /* Accumulate the actual arrival time */
        acum_arrival_time += act_arrival_time;

        /* Obtain the interarrival time quantised to the previous Ts */
        interarrival_time = ((act_arrival_time / Ts) * Ts);

        /* Quantise the arrival time to the previous Ts */
        arrival_time = ((acum_arrival_time / Ts) * Ts);

        /* Obtain the values for the analysis of the next job */
        cbs.computeNextData(finishing_time, arrival_time - interarrival_time, comp_time, interarrival_time);

      }
      else {

        /* Obtain the values for the analysis of the next job */
        cbs.computeNextData(finishing_time, arrival_time, comp_time, T);

      }

    }

    vector<times_t> times = cbs.getJobsTiming();

    /* Present the header for the results */
    cout << "==============================================" << endl;
    cout << "=                   Results                  =" << endl;
    cout << "==============================================" << endl;
    cout << setw(11) << "Deadline" << setw(23) << "Probability" << endl;

    /* Iterate for every deadline */
    while (num_hit < max_len) {

      num_hit = 0;

      /* Iterate over the vector of times */
      for (size_t i = 0; i < times.size(); i++) {

        /* The job finished before the deadline */
        if (times[i].response <= D) {

          num_hit++;

        }

      }

      /* Create the structure with the results */
      results_t tmp_results;
      tmp_results.deadline = D;
      tmp_results.num_hits = num_hit;
      tmp_results.num_jobs = max_len;

      /* Store the relevant results */
      cbs.insertResults(tmp_results);

      /* Present the results */
      cout << setw(11) << fixed << D
           << setw(29) << setprecision(20) <<  (double)num_hit / (double)max_len
           << setprecision(2) << endl;

      /* Update the relative deadline */
      D += Ts;

    }

    /* Store the results */
    cbs.saveResults(path_results, path_times);

    /* Obtain the final computation time */
    t_end = my_get_time();

    /* Present the final results */
    cout << "==============================================" << endl;
    cout << "=              Computation Time              =" << endl;
    cout << "==============================================" << endl;
    cout << setw(17) << "Parsing time:"  << setw(18) << t_solution_start - t_start << " us" << endl;
    cout << setw(18) << "Solution time:" << setw(17) << t_end - t_solution_start   << " us" << endl;
    cout << setw(15) << "Total time:"    << setw(20) << t_end - t_start            << " us" << endl;
    cout << "==============================================" << endl;

  }
  catch (Exc &e) {

    cerr << "Exception caught" << endl;
    e.what();

  }

  return 0;

}
