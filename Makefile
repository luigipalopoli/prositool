UNAME := $(shell uname)

CC = g++
CXXFLAGS = -Wall -Wextra -pedantic -g -std=c++11 -O3

ifeq ($(UNAME), Linux)
CPPFLAGS = -I/usr/include/eigen3 -I./utils/tinyxml2
endif

ifeq ($(UNAME), Darwin)
CPPFLAGS = -I/opt/local/include/eigen3 -I./utils/tinyxml2
endif

LDFLAGS = -L./ 
LDLIBS = -lprosit

APP1 = cli_solver
APP2 = xml_solver
APP3 = mctm_learner
APP4 = mctm_decoder
APP5 = mctm_autocorrelation
APP6 = cbs_simulator
APPS = $(APP1) $(APP2) $(APP3) $(APP4) $(APP5) $(APP6)

PROSIT_OBJS = ./utils/auxiliary_functions.o \
              ./utils/help_messages.o \
              ./utils/distribution.o \
              ./utils/pmf.o \
              ./utils/cdf.o \
              ./tasks/generic_task_descriptor.o \
              ./tasks/rr_task_descriptor.o \
              ./tasks/fp_task_descriptor.o \
              ./tasks/fp_task_schedule.o \
              ./solver/probability_solver.o \
              ./solver/rr_probability_solver.o \
              ./solver/fp_probability_solver.o \
              ./solver/analytic_rr_probability_solver.o \
              ./solver/companion_rr_probability_solver.o \
              ./solver/qbd_rr_probability_solver.o \
              ./solver/lt_qbd_rr_probability_solver.o \
              ./solver/cr_qbd_rr_probability_solver.o \
              ./qos/linear_qos_function.o \
              ./qos/quadratic_qos_function.o \
              ./optimisation/generic_budget_optimiser.o \
              ./optimisation/infinity_norm_budget_optimiser.o \
              ./xml/xml_parser.o \
              ./xml/xml_utils.o \
              ./mctm/baumwelch.o \
              ./mctm/viterbi.o \
              ./simulator/cbs.o \
              ./utils/tinyxml2/tinyxml2.o

CLI_SOLVER_OBJS = ./cli/cli_solver.o
XML_SOLVER_OBJS = ./xml/xml_solver.o
MCTM_LEARNER_OBJS = ./mctm/mctm_learner.o
MCTM_DECODER_OBJS = ./mctm/mctm_decoder.o
MCTM_AUTOCORR_OBJS = ./mctm/mctm_autocorrelation.o
CBS_SIMULATOR_OBJS = ./simulator/cbs_simulator.o

all: $(APPS)

libprosit.a: $(PROSIT_OBJS)
	ar rc $@ $(PROSIT_OBJS)
	ranlib $@

$(APP1): $(CLI_SOLVER_OBJS) libprosit.a
	$(CC) ${CXXFLAGS} $(CLI_SOLVER_OBJS) -o cli_solver $(LDLIBS) $(LDFLAGS)

$(APP2): $(XML_SOLVER_OBJS) libprosit.a
	$(CC) ${CXXFLAGS} $(XML_SOLVER_OBJS) -o xml_solver $(LDLIBS) $(LDFLAGS)

$(APP3): $(MCTM_LEARNER_OBJS) libprosit.a
	$(CC) ${CXXFLAGS} $(MCTM_LEARNER_OBJS) -o mctm_learner $(LDLIBS) $(LDFLAGS)

$(APP4): $(MCTM_DECODER_OBJS) libprosit.a
	$(CC) ${CXXFLAGS} $(MCTM_DECODER_OBJS) -o mctm_decoder $(LDLIBS) $(LDFLAGS)

$(APP5): $(MCTM_AUTOCORR_OBJS) libprosit.a
	$(CC) ${CXXFLAGS} $(MCTM_AUTOCORR_OBJS) -o mctm_autocorrelation $(LDLIBS) $(LDFLAGS)

$(APP6): $(CBS_SIMULATOR_OBJS) libprosit.a
	$(CC) ${CXXFLAGS} $(CBS_SIMULATOR_OBJS) -o cbs_simulator $(LDLIBS) $(LDFLAGS)

.PHONY: clean

clean: 
	rm -f ./solver/*.o ./solver/*.d ./cli/*.o ./cli/*.d ./simulator/*.o ./simulator/*.d ./qos/*.o ./qos/*.d ./optimisation/*.o ./optimisation/*.d ./tasks/*.o ./tasks/*.d ./xml/*.o ./xml/*.d ./utils/*.o ./utils/*.d ./utils/tinyxml2/*.o ./utils/tinyxml2/*.d ./mctm/*.o ./mctm/*.d *.a *~ $(APPS)

%.d: %.cpp
	$(CC) $(CXXFLAGS) -MM -MF $@ $<

-include $(PROSIT_OBJS:.o=.d)
-include $(CLI_SOLVER_OBJS:.o=.d)
-include $(XML_SOLVER_OBJS:.o=.d)
-include $(MCTM_LEARNER_OBJS:.o=.d)
-include $(MCTM_DECODER_OBJS:.o=.d)
-include $(MCTM_AUTOCORR_OBJS:.o=.d)
-include $(CBS_SIMULATOR_OBJS:.o=.d)
