/*!
 * @file    mctm_decoder.cpp
 * 
 * @brief   A CLI program for the verification of estimated HMMs parameters.
 * 
 * @author  Luca Abeni               <luca.abeni@santannapisa.it>
 *          Bernardo Villalba Frías  <b.r.villalba.frias@hva.nl>
 * 
 * @version 2.0
 * 
 * @date    30 November 2018
 * 
 * This program is a free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation.
 */
#include <getopt.h>

#include "viterbi.hpp"

using namespace PrositHmm;
using namespace PrositAux;
using namespace Eigen;
using namespace std;

static string path_observations;
static string path_transition;
static string path_emissions;
static string path_states = "comp_time_state";
static int64_t num_states = 0;
static uint64_t granularity = 1;
static double confidence = 0.05;

/* Parse the command line arguments */
static int opts_parse(int argc, char *argv[]) {

  int opt;

  /* Short description for the arguments */
  static const char short_options[] = "o:t:e:g:s:l:c:h";

  /* Long description for the arguments */
  static struct option long_options[] = {

    {"observation_path",   required_argument, 0, 'o'},
    {"transition_path",    required_argument, 0, 't'},
    {"emission_path",      required_argument, 0, 'e'},
    {"granularity",        required_argument, 0, 'g'},
    {"num_states",         required_argument, 0, 's'},
    {"states_path",        required_argument, 0, 'c'},
    {"significance",       required_argument, 0, 'l'},
    {"help",               no_argument,       0, 'h'},
    {0, 0, 0, 0},

  };

  /* Iterate over the list of the arguments */
  while ((opt = getopt_long(argc, argv, short_options, long_options, 0)) != -1) {

    switch (opt) {

      /* Path to the observation samples */
      case 'o':
        path_observations = optarg;
        break;

      /* Path to the transition matrix */
      case 't':
        path_transition = optarg;
        break;

      /* Path to the emission matrix */
      case 'e':
        path_emissions = optarg;
        break;

      /* Path to the classified observations */
      case 'c':
        path_states = optarg;
        break;

      /* Granularity */
      case 'g':
        granularity = atoi(optarg);
        break;

      /* Number of states */
      case 's':
        num_states = atoi(optarg);
        break;

      /* Number of states */
      case 'l':
        confidence = atof(optarg);
        break;

      /* Print the help */
      case 'h':
        help_decoder();
        break;

      default:
        EXC_PRINT("ERROR: Incorrect parameters during the parsing.");

    }

  }

  return optind;

}

/* Main program */
int main(int argc, char *argv[]) {

  /* Variables for the time analysis */
  long long t_start = 0, t_solution_start = 0, t_end = 0;

  /* Vector with the computation times */
  vector<uint64_t> observations;

  /* Variable for the result of the p-value */
  string passed;

  /* Variables for the sample ranges */
  uint64_t min_obs = numeric_limits<uint64_t>::max();
  uint64_t max_obs = numeric_limits<uint64_t>::min();

  try {

    /* Obtain the initial computation time */
    t_start = my_get_time();

    /* Parse the command line arguments */
    opts_parse(argc, argv);

    /* Check the number of states */
    if (num_states <= 0) {
      EXC_PRINT("ERROR: The number of states has not been properly set.");
    }

    /* Load the observation samples */
    if (!path_observations.empty()) {
      loadObservations(path_observations, observations, min_obs, max_obs, granularity);
    }
    else {
      EXC_PRINT("ERROR: The file with the observations is missing.");
    }

    MatrixXd transition_matrix = MatrixXd::Zero(num_states, num_states);

    /* Load the probability transition matrix */
    if (!path_transition.empty()) {
      loadMatrix(path_transition, transition_matrix);
    }
    else {
      EXC_PRINT("ERROR: The probability transition matrix is missing.");
    }

    MatrixXd emission_matrix = MatrixXd::Zero(num_states, max_obs - min_obs + 1);

    /* Load the probability transition matrix */
    if (!path_emissions.empty()) {
      loadEmissionMatrix(path_emissions, emission_matrix, min_obs);
    }
    else {
      EXC_PRINT("ERROR: The probability emission matrix is missing.");
    }

    /* Obtain the initial computation time */
    t_solution_start = my_get_time();

    /* Create the Viterbi algorithm */
    Viterbi v_algorithm;

    /* Obtain the sequence of states */
    v_algorithm.identifyStates(observations, transition_matrix, emission_matrix, min_obs);

    /* Classify the observations into states */
    vector<vector<uint64_t>> vec_states = v_algorithm.classifyObservations(num_states);

    /* Present the header for the results */
    cout << "===========================================================================" << endl;
    cout << "=                                 Results                                 =" << endl;
    cout << "===========================================================================" << endl;
    cout << setw(14) << "State" << setw(20) << "Z-statistic" << setw(15) << "P-value" << setw(13) << "p > " << confidence << endl;

    /* Save the classified observations */
    for (size_t i = 0; i < vec_states.size(); i++) {

      /* Variable for the name of the pmf file */
      char state_file[250];

      /* Build the string with the name */
      sprintf(state_file, "%s%ld.txt", path_states.c_str(), i + 1);

      /* Save the states into a file */
      saveObservations(state_file, vec_states[i]);

      /* Perform the independence test */
      double z = v_algorithm.runTest(vec_states[i]);

      /* Obtain the p-value */
      double p = v_algorithm.obtainPValue(z);

      /* Present the results */
      cout << setw(12) << fixed << i + 1
           << setw(20) << setprecision(4) << z 
           << setw(16) << setprecision(4)  << p
           << setw(18);

      /* Check whether the test was passed */
      if (p > confidence) {

        passed = "PASSED";

        /* Set the color to print out */
        if (isatty(fileno(stdout)))
          cout << "\033[1;32m";

      }
      else {

        passed = "FAILED";

        /* Set the color to print out */
        if (isatty(fileno(stdout)))
          cout << "\033[1;31m";

      }

      cout << passed;

      /* Restore the default color */
      if (isatty(fileno(stdout)))
        cout << "\033[0m";

      cout << endl;

    }

    /* Obtain the initial computation time */
    t_end = my_get_time();

    /* Present the final results */
    cout << "===========================================================================" << endl;
    cout << "=                            Computation  Time                            =" << endl;
    cout << "===========================================================================" << endl;
    cout << setw(17) << "Parsing time:"  << setw(18) << t_solution_start - t_start << " us" << endl;
    cout << setw(18) << "Solution time:" << setw(17) << t_end - t_solution_start   << " us" << endl;
    cout << setw(15) << "Total time:"    << setw(20) << t_end - t_start            << " us" << endl;
    cout << "===========================================================================" << endl;

  }
  catch (Exc &e) {

    cerr << "Exception caught" << endl;
    e.what();

  }

  return 0;

}
