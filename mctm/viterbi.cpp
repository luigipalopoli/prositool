/*!
 * @file    viterbi.cpp
 * 
 * @brief   This class defines an implementation for the Viterbi algorithm.
 * 
 * @author  Luca Abeni               <luca.abeni@santannapisa.it>
 *          Bernardo Villalba Frías  <b.r.villalba.frias@hva.nl>
 * 
 * @version 2.0
 * 
 * @date    30 November 2018
 * 
 * This program is a free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation.
 */
#include "viterbi.hpp"

using namespace Eigen;
using namespace std;

namespace PrositHmm {

  /// @brief Identify the state sequence.
  void Viterbi::identifyStates(const vector<uint64_t> &observations, const MatrixXd &transition_matrix, const MatrixXd &emission_matrix, uint64_t cmin) {

    /* Obtain the number of states */
    int64_t num_states = transition_matrix.cols();
    size_t num_samples = observations.size();

    /* Generate the initial distribution of the hidden chain */
    VectorXd nu = VectorXd::Ones(num_states) / num_states;

    MatrixXd v = MatrixXd::Zero(num_samples, num_states);
    MatrixXd prev = MatrixXd::Zero(num_samples, num_states);
    double max;

    for (int64_t s = 0; s < num_states; s++) {
      v(0, s) = nu(s) * emission_matrix(s, observations[0] - cmin);
    }

    for (size_t t = 1; t < num_samples; t++) {

      double sum = 0;

      for (int64_t s = 0; s < num_states; s++) {

        max = 0;

        for (int64_t prev_s = 0; prev_s < num_states; prev_s++) {

          double val = v(t - 1, prev_s) * transition_matrix(prev_s, s);

          if (val > max) {
            max = val;
            prev(t, s) = prev_s;
          }

        }

        v(t, s) = max * emission_matrix(s, observations[t] - cmin);
        sum += v(t, s);

      }

      /* Normalize the vector */
      for (int64_t s = 0; s < num_states; s++) {
        v(t, s) = v(t, s) / sum;
      }

    }

    states = MatrixXd::Zero(num_samples, 2);
    max = 0;
    for (int64_t s = 0; s < num_states; s++) {

      if (v(num_samples - 1, s) > max) {
        max = v(num_samples - 1, s);
        states(num_samples - 1, 0) = observations[num_samples - 1];
        states(num_samples - 1, 1) = s;
      }

    }

    for (int64_t t = num_samples - 1; t > 0; t--) {
      states(t - 1, 0) = observations[t - 1];
      states(t - 1, 1) = prev(t, states(t, 1));
    }

  }

  /// @brief Classify the observations into states.
  vector<vector<uint64_t>> Viterbi::classifyObservations(int64_t num_states) {

    vector<vector<uint64_t>> vec_states;

    /* Iterate over the states */
    for (int64_t i = 0; i < num_states; i++) {

      vector<uint64_t> seq;

      /* Iterate over the observations */
      for (int64_t t = 0; t < states.rows(); t++) {

        /* Check the corresponding state */
        if (states(t, 1) == i) {

          /* Insert the observation */
          seq.push_back(states(t, 0));

        }
        
      }

      /* Insert the vector of observations */
      vec_states.push_back(seq);

    }

    return vec_states;

  }

  /// @brief Perform the runtest for sample independence.
  double Viterbi::runTest(const vector<uint64_t> &observations) {

    size_t samples = observations.size();
    uint64_t n1 = 0, n2 = 0, r = 0;
    int64_t state = 0;
    double avg = 0.0, mu = 0.0, sigma = 0.0;

    /* Sum the samples */
    for (size_t i = 0; i < samples; i++) {

      avg += observations[i];

    }

    /* Compute the mean of the samples */
    avg /= (double)samples;

    /* Iterate over the samples */
    for (size_t i = 0; i < samples; i++) {

      /* Count the number of samples above the mean */
      if (observations[i] >= avg) {

        n1++;

        /* Update the number of runs */
        if (state != 1) {

          r++;
          state =  1;

        }

      }

      /* Count the number of samples below the mean */
      if (observations[i] <  avg) {

        n2++;

        /* Update the number of runs */
        if (state != -1) {

          r++;
          state = -1;

        } 

      }

    }

    /* Obtain the mean and the deviation */
    mu = ((2.0 * n1 * n2) / (n1 + n2)) + 1;
    sigma = sqrt((2.0 * n1 * n2 * ((2.0 * n1 * n2) - n1 - n2)) / ((n1 + n2) * (n1 + n2) * (n1 + n2 - 1)));

    /* Return the z-score */
    return (r - mu) / sigma;

  }

  /// @brief Obtain the p-value.
  double Viterbi::obtainPValue(double score) {

    /* Return the p-value */
    return 0.5 * erfc(-score * M_SQRT1_2);

  }

}
