/*!
 * @file    viterbi.hpp
 * 
 * @brief   This class defines a header for the Viterbi algorithm.
 * 
 * @author  Luca Abeni               <luca.abeni@santannapisa.it>
 *          Bernardo Villalba Frías  <b.r.villalba.frias@hva.nl>
 * 
 * @version 2.0
 * 
 * @date    30 November 2018
 * 
 * This program is a free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation.
 */
#ifndef VITERBI_HPP
#define VITERBI_HPP

#include "../utils/auxiliary_functions.hpp"
#include "../utils/help_messages.hpp"

namespace PrositHmm {

  class Viterbi {

    private:

      ///< Matrix of observation with the corresponding likely state.
      Eigen::MatrixXd states;

    public:

      /// @brief Constructor.
      ///
      /// This is the constructor for the Viterbi algoritm.
      Viterbi() : states() { }

      /// @brief Destructor.
      ///
      /// This is the destructor for the Viterbi algoritm.
      ~Viterbi() { }

      /// @brief Identify the state sequence.
      ///
      /// This function identifies the most likely sequence of internal 
      /// (hidden) states corresponding to the sequence of observations.
      ///
      /// @param observations is the vector of observations.
      /// @param transition_matrix is the transition probability matrix.
      /// @param emission_matrix is the emissions probability matrix.
      /// @param cmin is the minimum value of the observations.
      void identifyStates(const std::vector<uint64_t> &observations, const Eigen::MatrixXd &transition_matrix, const Eigen::MatrixXd &emission_matrix, uint64_t cmin);

      /// @brief Classify the observations into states.
      ///
      /// This function classifies the observations into the likely states.
      /// Each observation is inserted into the vector associated with the 
      /// state. Finally, a vector collects all the per-state vector and
      /// returns it.
      ///
      /// @param num_states is the number of states.
      ///
      /// @return vec_states is a vector of vectors of observations.
      std::vector<std::vector<uint64_t>> classifyObservations(int64_t num_states);

      /// @brief Perform the runtest for sample independence.
      ///
      /// This function performs the runtest for sample independence.
      ///
      /// [1] R. Liu, A. F. Mills, and J. H. Anderson. Independence thresholds: 
      ///     Balancing tractability and practicality in soft real-time 
      ///     stochastic analysis. In Real-Time Systems Symposium (RTSS), 2014 
      ///     IEEE, pages 314-323. IEEE, 2014.
      ///
      /// @param observations is the sample observations.
      ///
      /// @return score is the z-score of the test.
      double runTest(const std::vector<uint64_t> &observations);

      /// @brief Obtain the p-value.
      ///
      /// This function computes the p-value associated to a z-score for a
      /// normalized Gaussian.
      ///
      /// @param score is the z-score.
      ///
      /// @return value is the p-value.
      double obtainPValue(double score);

  };

}

#endif
