/*!
 * @file    baumwelch.hpp
 * 
 * @brief   This class defines a header for the Baum-Welch algorithm for 
 *          discrete hidden Markov models.
 * 
 * @author  Luca Abeni               <luca.abeni@santannapisa.it>
 *          Bernardo Villalba Frías  <b.r.villalba.frias@hva.nl>
 * 
 * @version 2.0
 * 
 * @date    30 November 2018
 * 
 * @note    This code is based on the original code proposed by Aurelien
 *          Garivier and Julius Su. Some minor modifications have been
 *          performed in order to port the code to C++11 and Eigen 3.
 * 
 * @author  Aurelien Garivier, CNRS & Telecom Paristech
 *          Julius Su,         Caltech (small bug corrections)
 * 
 * @date    January 2012
 * 
 * @link    http://www.telecom-paristech.fr/~garivier/code/index.html
 * 
 * This program is a free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation.
 */
#ifndef BAUM_WELCH_HPP
#define BAUM_WELCH_HPP

#include "../utils/auxiliary_functions.hpp"

namespace PrositHmm {

  class BaumWelch {

    private:

      Eigen::VectorXd c;      ///< Conditional likelihood.
      Eigen::MatrixXd phi;    ///< Forward probabilities (filter).
      Eigen::MatrixXd beta;   ///< Backward probabilities (smoothing factors).
      double tolerance;       ///< Tolerance for the stopping criterion.
      int64_t max_iter;       ///< Maximum number of iterations.

      /// @brief Compute the forward probabilities.
      ///
      /// This function computes the forward probabilities alpha divided by
      /// alpha_T(0), performing an HMM filtering of an observation sequence.
      ///
      /// @param observations is the vector of observations.
      /// @param transition_matrix is the transition probability matrix.
      /// @param emission_matrix is the emissions probability matrix.
      void hmmFilter(const std::vector<uint64_t> &observations, const Eigen::MatrixXd &transition_matrix, const Eigen::MatrixXd &emission_matrix, uint64_t cmin);

      /// @brief Compute the backward probabilities.
      ///
      /// This function computes the posterior distribution of the hidden 
      /// states
      ///
      /// @param observations is the vector of observations.
      /// @param transition_matrix is the transition probability matrix.
      /// @param emission_matrix is the emissions probability matrix.
      void hmmSmoother(const std::vector<uint64_t> &observations, const Eigen::MatrixXd &transition_matrix, const Eigen::MatrixXd &emission_matrix, uint64_t cmin);

    public:

      /// @brief Constructor.
      ///
      /// This is the constructor for the Baum-Welch algoritm.
      BaumWelch(double toleranced, 
          int64_t max_iterationd) : 
          c(),
          phi(),
          beta(),
          tolerance(toleranced),
          max_iter(max_iterationd) { }

      /// @brief Destructor.
      ///
      /// This is the destructor for the Baum-Welch algoritm.
      ~BaumWelch() { }

      /// @brief Compute maximum likehood estimates.
      ///
      /// This function computes maximum likehood estimates using 
      /// Expectation-Maximization iterations.
      ///
      /// @param observations is the vector of observations.
      /// @param transition_matrix is the transition probability matrix.
      /// @param emission_matrix is the emissions probability matrix.
      /// @param cmin is the minimum value of the observations.
      ///
      /// @return l is the log-likelihood of the samples for the given parameters.
      double computeHMMParameters(const std::vector<uint64_t> &observations, Eigen::MatrixXd &transition_matrix, Eigen::MatrixXd &emission_matrix, uint64_t cmin);

  };

}

#endif
