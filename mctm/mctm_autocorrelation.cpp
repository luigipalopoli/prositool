/*!
 * @file    mctm_autocorrelation.cpp
 * 
 * @brief   A CLI program for autocorrelation function.
 * 
 * @author  Luca Abeni               <luca.abeni@santannapisa.it>
 *          Bernardo Villalba Frías  <b.r.villalba.frias@hva.nl>
 * 
 * @version 2.0
 * 
 * @date    30 November 2018
 * 
 * This program is a free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation.
 */
#include <getopt.h>

#include "../utils/auxiliary_functions.hpp"
#include "../utils/help_messages.hpp"

using namespace PrositAux;
using namespace Eigen;
using namespace std;

static string path_observations;
static string path_autocorrelation = "autocorrelation.txt";
static uint64_t lags = 20;

/* Parse the command line arguments */
static int opts_parse(int argc, char *argv[]) {

  int opt;

  /* Short description for the arguments */
  static const char short_options[] = "o:l:d:h";

  /* Long description for the arguments */
  static struct option long_options[] = {

    {"observation_path",   required_argument, 0, 'o'},
    {"destination_path",   required_argument, 0, 'd'},
    {"lags",               required_argument, 0, 'l'},
    {"help",               no_argument,       0, 'h'},
    {0, 0, 0, 0},

  };

  /* Iterate over the list of the arguments */
  while ((opt = getopt_long(argc, argv, short_options, long_options, 0)) != -1) {

    switch (opt) {

      /* Path to the observation samples */
      case 'o':
        path_observations = optarg;
        break;

      /* Path to the observation samples */
      case 'l':
        lags = atoi(optarg);
        break;

      /* Path to the observation samples */
      case 'd':
        path_autocorrelation = optarg;
        break;

      /* Print the help */
      case 'h':
        help_autocorr();
        break;

      default:
        EXC_PRINT("ERROR: Incorrect parameters during the parsing.");

    }

  }

  return optind;

}

VectorXd autocorrelationFunction(const vector<uint64_t> &observations) {

  size_t samples = observations.size();
  VectorXd autocorr(lags + 1);
  double avg = 0.0;

  for (size_t t = 0; t < samples; t++) {
    avg = avg + observations[t];
  }
  avg /= (double)samples;

  for (uint64_t t = 0; t < (lags + 1); t++) {

    autocorr(t) = 0.0;

    if (t < samples) {

      for (size_t tau = 0; tau < (samples - t); tau++) {

        autocorr(t) += (observations[tau] - avg) * (observations[tau + t] - avg);

      }

      if (t) {
        autocorr(t) = autocorr(t) / autocorr(0);
      }

    }

  }

  autocorr(0) = 1.0;

  return autocorr;

}

/* Main program */
int main(int argc, char *argv[]) {

  /* Variables for the time analysis */
  long long t_start = 0, t_solution_start = 0, t_end = 0;

  /* Vector with the computation times */
  vector<uint64_t> observations;

  /* Variables for the sample ranges */
  uint64_t min_obs = numeric_limits<uint64_t>::max();
  uint64_t max_obs = numeric_limits<uint64_t>::min();

  try {

    /* Obtain the initial computation time */
    t_start = my_get_time();

    /* Parse the command line arguments */
    opts_parse(argc, argv);

    /* Load the observation samples */
    if (!path_observations.empty()) {
      loadObservations(path_observations, observations, min_obs, max_obs, 1);
    }
    else {
      EXC_PRINT("ERROR: The file with the observations is missing.");
    }

    /* Obtain the initial computation time */
    t_solution_start = my_get_time();

    /* Obtain the autocorrelation */
    VectorXd autocorrelation = autocorrelationFunction(observations);

    /* Save the PMF of the state */
    saveMatrix(path_autocorrelation, autocorrelation, 0, lags);

    /* Present the header for the results */
    cout << "==========================================" << endl;
    cout << "=                 Results                =" << endl;
    cout << "==========================================" << endl;
    cout << setw(11) << "Lag" << setw(23) << "Autocorrelation" << endl;

    /* Save the classified observations */
    for (int64_t i = 0; i < autocorrelation.rows(); i++) {

      cout << setw(11) << fixed << i
           << setw(18) << setprecision(4) << autocorrelation[i] << endl;

    }

    /* Obtain the initial computation time */
    t_end = my_get_time();

    /* Present the final results */
    cout << "==========================================" << endl;
    cout << "=            Computation Time            =" << endl;
    cout << "==========================================" << endl;
    cout << setw(17) << "Parsing time:"  << setw(18) << t_solution_start - t_start << " us" << endl;
    cout << setw(18) << "Solution time:" << setw(17) << t_end - t_solution_start   << " us" << endl;
    cout << setw(15) << "Total time:"    << setw(20) << t_end - t_start            << " us" << endl;
    cout << "==========================================" << endl;

  }
  catch (Exc &e) {

    cerr << "Exception caught" << endl;
    e.what();

  }

  return 0;

}
