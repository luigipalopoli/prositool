/*!
 * @file    mctm_learner.cpp
 * 
 * @brief   A CLI program for the estimation of the HMMs parameters.
 * 
 * @author  Luca Abeni               <luca.abeni@santannapisa.it>
 *          Bernardo Villalba Frías  <b.r.villalba.frias@hva.nl>
 * 
 * @version 2.0
 * 
 * @date    30 November 2018
 * 
 * This program is a free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation.
 */
#include <getopt.h>

#include "../utils/help_messages.hpp"
#include "baumwelch.hpp"

using namespace PrositHmm;
using namespace PrositAux;
using namespace Eigen;
using namespace std;

static string path_observations;
static string path_transition = "transition_matrix.txt";
static string path_emissions = "pmf_state";
static int64_t num_states = 0;
static uint64_t granularity = 1;
static int64_t num_trials = 10;
static int64_t num_iter = 1000;
static double eps = 1e-4;

/* Parse the command line arguments */
static int opts_parse(int argc, char *argv[]) {

  int opt;

  /* Short description for the arguments */
  static const char short_options[] = "o:t:e:g:s:r:i:p:h";

  /* Long description for the arguments */
  static struct option long_options[] = {

    {"observation_path",   required_argument, 0, 'o'},
    {"transition_path",    required_argument, 0, 't'},
    {"emission_path",      required_argument, 0, 'e'},
    {"granularity",        required_argument, 0, 'g'},
    {"num_states",         required_argument, 0, 's'},
    {"num_trials",         required_argument, 0, 'r'},
    {"num_iterations",     required_argument, 0, 'i'},
    {"epsilon",            required_argument, 0, 'p'},
    {"help",               no_argument,       0, 'h'},
    {0, 0, 0, 0},

  };

  /* Iterate over the list of the arguments */
  while ((opt = getopt_long(argc, argv, short_options, long_options, 0)) != -1) {

    switch (opt) {

      /* Path to the observation samples */
      case 'o':
        path_observations = optarg;
        break;

      /* Path to the transition matrix */
      case 't':
        path_transition = optarg;
        break;

      /* Path to the emission matrix */
      case 'e':
        path_emissions = optarg;
        break;

      /* Granularity */
      case 'g':
        granularity = atoi(optarg);
        break;

      /* Number of states */
      case 's':
        num_states = atoi(optarg);
        break;

      /* Number of trials */
      case 'r':
        num_trials = atoi(optarg);
        break;

      /* Number of trials */
      case 'i':
        num_iter = atoi(optarg);
        break;

      /* Number of trials */
      case 'p':
        eps = atof(optarg);
        break;

      /* Print the help */
      case 'h':
        help_learner();
        break;

      default:
        EXC_PRINT("ERROR: Incorrect parameters during the parsing.");

    }

  }

  return optind;

}

/* Main program */
int main(int argc, char *argv[]) {

  /* Variables for the time analysis */
  long long t_start = 0, t_solution_start = 0, t_end = 0;

  /* Vector with the computation times */
  vector<uint64_t> observations;

  /* Variables for the sample ranges */
  uint64_t min_obs = numeric_limits<uint64_t>::max();
  uint64_t max_obs = numeric_limits<uint64_t>::min();

  double trial_log_likelihood = 0.0;
  double log_likelihood = -1e300;
  int64_t best_rep = 0;

  try {

    /* Obtain the initial computation time */
    t_start = my_get_time();

    /* Parse the command line arguments */
    opts_parse(argc, argv);

    /* Check the number of states */
    if (num_states <= 0) {
      EXC_PRINT("ERROR: The number of states has not been properly set.");
    }

    /* Load the observation samples */
    if (!path_observations.empty()) {
      loadObservations(path_observations, observations, min_obs, max_obs, granularity);
    }
    else {
      EXC_PRINT("ERROR: The file with the computation times is missing.");
    }

    MatrixXd transition_matrix = MatrixXd::Zero(num_states, num_states);
    MatrixXd emission_matrix = MatrixXd::Zero(num_states, max_obs - min_obs + 1);

    /* Initialize the seed for the random generator */
    srand(time(NULL));

    /* Obtain the initial computation time */
    t_solution_start = my_get_time();

    /* Present the header for the results */
    cout << "=================================================================================" << endl;
    cout << "=                                    Results                                    =" << endl;
    cout << "=================================================================================" << endl;
    cout << setw(15) << "Iteration" << setw(21) << "Trial likelihood" << setw(20) << "Best likelihood" << setw(19) << "Best iteration" << endl;

    /* Repeat the given number of trials */
    for (int64_t i = 0; i < num_trials; i++) {

      /* Generate the probability transition matrix */
      MatrixXd trans_matrix = generateProbabilityMatrix(num_states, num_states);

      /* Generate the probability emission matrix */
      MatrixXd emis_matrix = generateProbabilityMatrix(num_states, max_obs - min_obs + 1);

      /* Create the Baul-Welch algorithm */
      BaumWelch bw_algorithm(eps, num_iter);

      /* Obtain the HMM parameters */
      trial_log_likelihood = bw_algorithm.computeHMMParameters(observations, trans_matrix, emis_matrix, min_obs);

      /* The general log-likelihood is improved */
      if (trial_log_likelihood > log_likelihood){

        /* Update the log-likelihood */
        log_likelihood = trial_log_likelihood;

        /* Update the transition and emission matrices */
        transition_matrix = trans_matrix;
        emission_matrix = emis_matrix;

        /* Store the best repetition */
        best_rep = i + 1;

      }

      cout << setw(11) << fixed << i + 1
           << setw(23) << setprecision(4) << trial_log_likelihood 
           << setw(20) << setprecision(4)  << log_likelihood
           << setw(14) << best_rep << endl;

    }

    /* Save the transition matrix of the MCTM */
    saveMatrix(path_transition, transition_matrix, 0, 0);

    /* Iterate over the number of states */
    for (int64_t i = 0; i < num_states; i++) {

      /* Variable for the name of the pmf file */
      char pmf_file[250];

      /* Build the string with the name */
      sprintf(pmf_file, "%s%ld.txt", path_emissions.c_str(), i + 1);

      /* Save the PMF of the state */
      saveMatrix(pmf_file, emission_matrix.row(i), min_obs, max_obs);

    }

    /* Obtain the initial computation time */
    t_end = my_get_time();

    /* Present the final results */
    cout << "=================================================================================" << endl;
    cout << "=                               Computation  Time                               =" << endl;
    cout << "=================================================================================" << endl;
    cout << setw(17) << "Parsing time:"  << setw(18) << t_solution_start - t_start << " us" << endl;
    cout << setw(18) << "Solution time:" << setw(17) << t_end - t_solution_start   << " us" << endl;
    cout << setw(15) << "Total time:"    << setw(20) << t_end - t_start            << " us" << endl;
    cout << "=================================================================================" << endl;

  }
  catch (Exc &e) {

    cerr << "Exception caught" << endl;
    e.what();

  }

  return 0;

}
