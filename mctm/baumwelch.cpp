/*!
 * @file    baumwelch.cpp
 * 
 * @brief   This class defines an implementation for the Baum-Welch algorithm
 *          for discrete hidden Markov models.
 * 
 * @author  Luca Abeni               <luca.abeni@santannapisa.it>
 *          Bernardo Villalba Frías  <b.r.villalba.frias@hva.nl>
 * 
 * @version 2.0
 * 
 * @date    30 November 2018
 * 
 * @note    This code is based on the original code proposed by Aurelien
 *          Garivier and Julius Su. Some minor modifications have been
 *          performed in order to port the code to C++11 and Eigen 3.
 * 
 * @author  Aurelien Garivier, CNRS & Telecom Paristech
 *          Julius Su,         Caltech (small bug corrections)
 * 
 * @date    January 2012
 * 
 * @link    http://www.telecom-paristech.fr/~garivier/code/index.html
 * 
 * This program is a free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation.
 */
#include "baumwelch.hpp"

using namespace Eigen;
using namespace std;

namespace PrositHmm {

  /// @brief Compute the forward probabilities.
  void BaumWelch::hmmFilter(const vector<uint64_t> &observations, const MatrixXd &transition_matrix, const MatrixXd &emission_matrix, uint64_t cmin) {

    /* Obtain the number of states */
    int64_t num_states = transition_matrix.cols();

    /* Generate the initial distribution of the hidden chain */
    VectorXd nu = VectorXd::Ones(num_states) / num_states;
    VectorXd z = VectorXd::Zero(num_states);

    /* Initialize the conditional likelihood */
    c(0) = 0;

    /* Obtain the initial forward probabilities */
    for (int64_t j = 0; j < num_states; j++) {
      z(j) = nu(j) * emission_matrix(j, observations[0] - cmin);
      c(0) += z(j);
    }

    for (int64_t j = 0; j < num_states; j++) {
      phi(0, j) = z(j) / c(0);
    }

    /* Obtain the next forward probabilities */
    for (size_t t = 1; t < observations.size(); t++) {

      c(t) = 0;

      for (int64_t j = 0; j < num_states; j++) {

        z(j) = 0;

        for (int64_t i = 0; i < num_states; i++) {
          z(j) += phi(t - 1, i) * transition_matrix(i, j) * emission_matrix(j, observations[t] - cmin);
        }

        c(t) += z(j);

      }

      for (int64_t j = 0; j < num_states; j++) {
        phi(t, j) = z(j) / c(t);
      }

    }

  }

  /// @brief Compute the backward probabilities.
  void BaumWelch::hmmSmoother(const vector<uint64_t> &observations, const MatrixXd &transition_matrix, const MatrixXd &emission_matrix, uint64_t cmin) {

    /* Obtain the number of states */
    int64_t num_states = transition_matrix.cols();

    for (int64_t j = 0; j < num_states; j++) {
      beta(observations.size() - 1, j) = 1;
    }

    for (int64_t t = observations.size() - 2; t >= 0; t--) {

      for (int64_t i = 0; i < num_states; i++) {

        double z = 0;

        for (int64_t j = 0; j < num_states; j++) {
          z += transition_matrix(i, j) * emission_matrix(j, observations[t + 1] - cmin) * beta(t + 1, j);
        }

        beta(t, i) = z / c(t + 1);

      }

    }

  }

  /// @brief Compute maximum likehood estimates.
  double BaumWelch::computeHMMParameters(const vector<uint64_t> &observations, MatrixXd &transition_matrix, MatrixXd &emission_matrix, uint64_t cmin) {

    /* Obtain the number of states */
    int64_t num_states = transition_matrix.cols();
    int64_t num_output = emission_matrix.cols();
    size_t samples = observations.size();

    // returns best log-likelihood found
    double change = tolerance + 1; 
    double z, l = 0.0;
    c = VectorXd(samples);
    phi = MatrixXd(samples, num_states);
    beta = MatrixXd(samples, num_states);

    for (int64_t it = 0; (change > tolerance) && (it < max_iter); it++) {

      MatrixXd A = MatrixXd::Zero(num_states, num_output);
      MatrixXd B = MatrixXd::Zero(num_states, num_states);
      VectorXd s = VectorXd::Zero(num_states);
      VectorXd s2 = VectorXd::Zero(num_states);

      change = 0;
      hmmFilter(observations, transition_matrix, emission_matrix, cmin);
      hmmSmoother(observations, transition_matrix, emission_matrix, cmin);

      for (size_t t = 0; t < samples; t++) {

        for (int64_t i = 0; i < num_states; i++) {

          z = phi(t, i) * beta(t, i);
          A(i, observations[t] - cmin) += z;
          s(i) += z;

        }

      }

      for (int64_t i = 0; i < num_states; i++) {

        for (int64_t j = 0; j < num_output; j++) {

          change = max(change, abs(emission_matrix(i, j) - A(i, j) / s(i)));
          emission_matrix(i, j) = A(i, j) / s(i);

        }

      }

      for(size_t t = 1; t < samples; t++) {

        for(int64_t i = 0; i < num_states; i++) {

          for(int64_t j = 0; j < num_states; j++) {

            z = phi(t - 1, i) * transition_matrix(i, j) * emission_matrix(j, observations[t] - cmin) * beta(t, j) / c(t);
            B(i, j) += z;
            s2(i) += z;

          }

        }

      }

      for (int64_t i = 0; i < num_states; i++) {

        for (int64_t j = 0; j < num_states; j++) {

          change = max(change, abs(transition_matrix(i, j) - B(i, j) / s2(i)));
          transition_matrix(i, j) = B(i, j) / s2(i);

        }

      }

      l = 0;

      for (size_t t = 0; t < samples; t++) {
        l += log(c(t));
      }

    }

    return l;

  }

}

