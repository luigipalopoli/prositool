/*!
 * @file    generic_budget_optimiser.hpp
 * 
 * @brief   This class defines a header for .
 * 
 * @author  Luigi Palopoli           <luigi.palopoli@unitn.it>
 *          Bernardo Villalba Frías  <b.r.villalba.frias@hva.nl>
 * 
 * @version 3.0
 * 
 * @date    30 November 2019
 * 
 * This program is a free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation.
 */
#ifndef GENERIC_BUDGET_OPTIMISER_HPP
#define GENERIC_BUDGET_OPTIMISER_HPP

#include "../utils/auxiliary_functions.hpp"
#include "../tasks/rr_task_descriptor.hpp"

namespace PrositCore {

  class GenericBudgetOptimiser {

    public:

      /// @brief Structure for the optimiser error codes.
      ///
      /// This structure contains the different error codes that can occur
      /// when optimising the budget.
      typedef enum ERR_CODES {
        NOT_EXECUTED, 
        OK, 
        BAD_LOWER_BOUNDS, 
        NO_SOLUTION
      } ERR_CODES;

    protected:

      std::vector<GenericTaskDescriptor*> tasks;
      ERR_CODES state;
      double optimum;
      bool verbose;

    public:

      double eps;
      double total_bandwidth;

      /// @brief Constructor.
      ///
      /// This is the constructor for the QoS function.
      ///
      /// @param v is the .
      /// @param epsd is the .
      /// @param total_bandwidthd is the .
      GenericBudgetOptimiser(std::vector<GenericTaskDescriptor*> v,
          double epsd,
          double total_bandwidthd) :
          tasks(v),
          state(NOT_EXECUTED),
          optimum(-1e38),
          verbose(false),
          eps(epsd),
          total_bandwidth(total_bandwidthd) { }

      /// @brief Destructor.
      ///
      /// This is the destructor for the optimiser.
      virtual ~GenericBudgetOptimiser() { }

      double getOptimum() const {
        return optimum;
      }

      ERR_CODES getState() const {
        return state;
      }

      bool setVerboseFlag(bool verbosed) {

        /* Store the old verbose flag */
        bool current = verbose;

        /* Set the new verbose flag */
        verbose = verbosed;

        return current;

      }

      void initTargetBounds();

      virtual double optimise() = 0;

  };

}

#endif
