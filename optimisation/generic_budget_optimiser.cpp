/*!
 * @file    generic_budget_optimiser.cpp
 * 
 * @brief   This class defines a header for .
 * 
 * @author  Luigi Palopoli           <luigi.palopoli@unitn.it>
 *          Bernardo Villalba Frías  <b.r.villalba.frias@hva.nl>
 * 
 * @version 3.0
 * 
 * @date    30 November 2019
 * 
 * This program is a free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation.
 */
#include "generic_budget_optimiser.hpp"

using namespace PrositAux;
using namespace std;

namespace PrositCore {
  
  void GenericBudgetOptimiser::initTargetBounds() {

    vector<GenericTaskDescriptor*>::iterator it;

    /* Iterate over the task in the taskset */
    for (it = tasks.begin() ; it != tasks.end(); it++) {

      ResourceReservationTaskDescriptor *t;

      if (!(t = dynamic_cast<ResourceReservationTaskDescriptor *>((*it)))) {
        EXC_PRINT_2("Impossible to cast task GenericTaskDescriptor to ResourceReservationTaskDescriptor for task ", (*it)->getName());
      }

      /* Check whether the bounds have been initialized */
      if (!(t->getBoundsInited())) {

        /* Present online information */
        if (verbose)
          cerr << "Initialising bounds for task " << t->getName() << endl;

        /* Set the minimum and maximum target for the QoS function */
        if (!(t->identifyBounds(t->getMinQoSTarget(), t->getMaxQoSTarget()))) {
          EXC_PRINT_2("ERROR: Cannot set the bounds for the task", t->getName());
        }

      }

    }

  }

}
