/*!
 * @file    infinity_norm_budget_optimiser.hpp
 * 
 * @brief   This class defines a header for .
 * 
 * @author  Luigi Palopoli           <luigi.palopoli@unitn.it>
 *          Bernardo Villalba Frías  <b.r.villalba.frias@hva.nl>
 * 
 * @version 3.0
 * 
 * @date    30 November 2019
 * 
 * This program is a free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation.
 */
#ifndef INFINITY_NORM_BUDGET_OPTIMISER_HPP
#define INFINITY_NORM_BUDGET_OPTIMISER_HPP

#include "generic_budget_optimiser.hpp"

namespace PrositCore {

  class InfinityNormBudgetOptimiser: public GenericBudgetOptimiser {

    public:

      /// @brief Constructor.
      ///
      /// This is the constructor for the optimiser.
      ///
      /// @param v is the .
      /// @param eps is the .
      /// @param total_bandwidth is the .
      InfinityNormBudgetOptimiser(std::vector<GenericTaskDescriptor*> v,
          double eps,
          double total_bandwidth) : 
          GenericBudgetOptimiser(v, eps, total_bandwidth) { }

      double optimise();

  };

}

#endif
