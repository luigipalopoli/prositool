/*!
 * @file    infinity_norm_budget_optimiser.hpp
 * 
 * @brief   This class defines a header for .
 * 
 * @author  Luigi Palopoli           <luigi.palopoli@unitn.it>
 *          Bernardo Villalba Frías  <b.r.villalba.frias@hva.nl>
 * 
 * @version 3.0
 * 
 * @date    30 November 2019
 * 
 * This program is a free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation.
 */
#include "infinity_norm_budget_optimiser.hpp"

using namespace PrositAux;
using namespace std;

namespace PrositCore {

  double InfinityNormBudgetOptimiser::optimise() {

    /* Obtain the number of the parsed tasks */
    size_t number = tasks.size();

    vector<uint64_t> working_budget(number);
    vector<bool> checked(number);

    double inf_norm_1 = 1e38, inf_norm_2 = 1e38, inf_norm = 0.0;
    double Btot = 0;

    vector<GenericTaskDescriptor*>::iterator it;

    for (it = tasks.begin(); it != tasks.end(); it++) {

      ResourceReservationTaskDescriptor *t;

      if (!(t = dynamic_cast<ResourceReservationTaskDescriptor *>((*it)))) {
        EXC_PRINT_2("Impossible to cast task GenericTaskDescriptor to ResourceReservationTaskDescriptor for task ", (*it)->getName());
      }

      if (!(t->getBoundsInited())) {

        if (verbose)
          cerr << "Initialising bounds for task " << t->getName() << endl;

        t->initBounds();

      }

      t->setVerboseFlag(verbose);
      inf_norm_1 = min(t->getQoSMax(), inf_norm_1);
      inf_norm_2 = min(t->getQoSMin(), inf_norm_2);
      Btot += double(t->getQMin()) / double(t->getServerPeriod());

    }

    if (Btot > (total_bandwidth + eps)) {
      state = BAD_LOWER_BOUNDS;
      return -1e38;
    }

    Btot = 0;
    uint64_t i = 0;
    double Breserved = 0.0;

    for (it = tasks.begin(); it != tasks.end(); it++) {

      ResourceReservationTaskDescriptor *t;

      if (!(t = dynamic_cast<ResourceReservationTaskDescriptor *>((*it)))) {
        EXC_PRINT_2("Impossible to cast task GenericTaskDescriptor to ResourceReservationTaskDescriptor for task ", (*it)->getName());
      }

      assert(i < number);
      assert(t->inverseQoSEvaluation(inf_norm_1, working_budget[i], true));

      if (working_budget[i] < t->getQMin()) {
        working_budget[i] = t->getQMin();
        checked[i] = true;
        Breserved += double(working_budget[i]) / double(t->getServerPeriod());
      }
      else {
        checked[i] = false;
      }

      working_budget[i] = max(t->getQMin(), working_budget[i]);
      Btot += double(working_budget[i]) / double(t->getServerPeriod());
      i++;

    }

    int counter = 0;

    if (Btot > (total_bandwidth + eps)) {

      while ((inf_norm_1 - inf_norm_2) > eps) {

        inf_norm = (inf_norm_2 +  inf_norm_1) / 2;
        i = 0;
        Btot = 0;

        if (verbose && (counter % 100)) {
          cerr << "Iteration #: " << counter << " reached, " << "difference between the bounds of the norms: " << inf_norm_1 - inf_norm_2 << endl;
        }

        vector<GenericTaskDescriptor*>::iterator it1;

        for (it1 = tasks.begin(); it1 != tasks.end(); it1++) {

          if (!checked[i]) {

            ResourceReservationTaskDescriptor *t1;

            if (!(t1 = dynamic_cast<ResourceReservationTaskDescriptor *>((*it1)))) {
              EXC_PRINT_2("Impossible to cast task GenericTaskDescriptor to ResourceReservationTaskDescriptor for task ", (*it1)->getName());
            }

            assert(t1->inverseQoSEvaluation(inf_norm, working_budget[i], false));

            if (working_budget[i] < t1->getQMin()) {

              working_budget[i] = t1->getQMin();
              checked[i] = true;
              Breserved += double(working_budget[i]) / double(t1->getServerPeriod());

            }

            Btot += double(working_budget[i]) / double(t1->getServerPeriod());

          }

          i++;

        }

        if ((Btot + Breserved) < (total_bandwidth - eps)) {
          inf_norm_2 = inf_norm;
        }
        else {
          inf_norm_1 = inf_norm;
        }

        counter++;

      } 

    }
    else {

      inf_norm = inf_norm_1;

    }

    i = 0;

    for (it = tasks.begin(); it != tasks.end(); it++) {

      ResourceReservationTaskDescriptor *t;

      if (!(t = dynamic_cast<ResourceReservationTaskDescriptor *>((*it)))) {
        EXC_PRINT_2("Impossible to cast task GenericTaskDescriptor to ResourceReservationTaskDescriptor for task ", (*it)->getName());
      }

      t->setBudget(working_budget[i]);

      /* Update the granularity to be the budget */
      if (strcmp(t->algorithm.c_str(), "analytic") == 0) {
        t->setGranularity(t->getBudget());
      }

      inf_norm = min(inf_norm, t->QoSEvaluation(working_budget[i]));

      if (verbose)
        cerr << "Setting budget to " << working_budget[i] << " with QoS " << t->QoSEvaluation(working_budget[i]) << " for Task " << t->getName() << endl;
        

      if ((working_budget[i] < t->getQMin()) || (working_budget[i] > t->getQMax())) {
        state = NO_SOLUTION;
        return -1e38;
      }

      i++;

    }

    state = OK;
    optimum = inf_norm;

    return inf_norm;

  }

}
