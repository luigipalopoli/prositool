/*!
 * @file    xml_solver.cpp
 * 
 * @brief   A XML solver for computation of stady state probabilities.
 * 
 * @author  Luigi Palopoli           <luigi.palopoli@unitn.it>
 *          Bernardo Villalba Frías  <b.r.villalba.frias@hva.nl>
 * 
 * @version 3.0
 * 
 * @date    30 November 2019
 * 
 * This program is a free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation.
 */
#include <getopt.h>

#include "../utils/auxiliary_functions.hpp"

#include "xml_parser.hpp"
#include "xml_utils.hpp"

using namespace PrositCore;
using namespace PrositAux;
using namespace std;

bool verbose_flag = false;
static string path_specification_file;
static string path_taskset_file;
long long  t_start = 0, t_xml_parse_end = 0;

/* Parse the command line arguments */
static int opts_parse(int argc, char *argv[]) {

  int opt;

  /* Short description for the arguments */
  static const char short_options[] = "f:t:vh";

  /* Long description for the arguments */
  static struct option long_options[] = {

      {"spec_file",   required_argument, 0, 'f'},
      {"taskset",     required_argument, 0, 't'},
      {"verbose",     no_argument,       0, 'v'},
      {"help",        no_argument,       0, 'h'},
      {0, 0, 0, 0},

  };

  /* Iterate over the list of the arguments */
  while ((opt = getopt_long(argc, argv, short_options, long_options, 0)) != -1) {

    switch (opt) {

      /* Path to the xml specification file */
      case 'f':
        path_specification_file = optarg;
        break;

      /* Path to the xml taskset file */
      case 't':
        path_taskset_file = optarg;
        break;

      /* Verbose flag */
      case 'v': 
        verbose_flag = true;
        break;

      /* Print the help */
      case 'h':
        help_xml();
        break;

      default: 
        EXC_PRINT("ERROR: Incorrect parameters during the parsing.");

    }

  }

  return optind;

}

/* Main program */
int main(int argc, char *argv[]) {

  try {

    /* Obtain the initial computation time */
    t_start = my_get_time();

    /* Parse the command line arguments */
    opts_parse(argc, argv);

    /* Check the proper number of arguments */
    if (path_specification_file.empty()) {
      EXC_PRINT("ERROR: The xml file with the specifications is missing.");
    }

    /* Check the proper number of arguments */
    if (path_taskset_file.empty()) {
      EXC_PRINT("ERROR: The xml file with the taskset is missing.");
    }

    /* Pointer to the XML parser */
    unique_ptr<Parser> p(new Parser(path_specification_file.c_str(), path_taskset_file.c_str()));

    /* Parse the xml file */
    ACTIONS act = p->parseFile();

    /* Present online information */
    if (verbose_flag) {
      cerr << "XML file successfully parsed." << endl;
    }

    /* Obtain the final parsing time */
    t_xml_parse_end = my_get_time();

    /* Perform the corresponding action */
    switch(act) {

      /* Optimization case */
      case ACTIONS::OPTIMISE:
        executeOptimisation(move(p));
        break;

      /* Analysis case */
      case ACTIONS::SOLVE:
        executeSolve(move(p));
        break;

      /* Default case */
      default:
        EXC_PRINT("ERROR: The action has not been recognised.");

    }

  }
  catch (Exc &e) {
    cerr << "Exception caught" << endl;
    e.what();
  }

  return 0;

}
