/*!
 * @file    xml_parser.hpp
 * 
 * @brief   This class defines a header for an XML parser for the steady state
 *          probabilities solver.
 * 
 * @author  Luigi Palopoli           <luigi.palopoli@unitn.it>
 *          Bernardo Villalba Frías  <b.r.villalba.frias@hva.nl>
 * 
 * @version 3.0
 * 
 * @date    30 November 2019
 * 
 * This program is a free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation.
 */
#ifndef XML_PARSER_HPP
#define XML_PARSER_HPP

#include <tinyxml2.h>

#include "../solver/companion_rr_probability_solver.hpp"
#include "../solver/analytic_rr_probability_solver.hpp"
#include "../solver/cr_qbd_rr_probability_solver.hpp"
#include "../solver/lt_qbd_rr_probability_solver.hpp"
#include "../solver/fp_probability_solver.hpp"
#include "../qos/quadratic_qos_function.hpp"
#include "../utils/auxiliary_functions.hpp"
#include "../tasks/rr_task_descriptor.hpp"
#include "../tasks/fp_task_descriptor.hpp"
#include "../qos/linear_qos_function.hpp"
#include "../tasks/fp_task_schedule.hpp"
#include "../utils/help_messages.hpp"

namespace PrositCore {

  /// @brief Structure for the solver actions.
  ///
  /// This structure contains the different actions that the solver can
  /// perform.
  typedef enum ACTIONS {
    OPTIMISE,
    SOLVE,
    NO_ACT
  } ACTIONS;

  /// @brief Structure for the types of optimization.
  ///
  /// This structure contains the different types of optimizations that can
  /// be applied.
  typedef enum OPT_TYPE {
    NO_OPT,
    INFINITY_NORM
  } OPT_TYPE;

  class Parser {

    private:

      tinyxml2::XMLDocument *spec;       ///< Pointer to the xml specification file.
      tinyxml2::XMLDocument *taskset;    ///< Pointer to the xml taskset file.
      ACTIONS act;                       ///< Action to be performed.
      OPT_TYPE opt;                      ///< Optimization type.
      double optim_eps;                  ///< Epsilon for the optimization.
      double total_bandwidth;            ///< Total bandwidth to be assigned.
      bool verbose;                      ///< Flag to print out online information.

      ///< Vector with all the parsed tasks.
      std::vector<GenericTaskDescriptor*> tsk_vect;
      std::vector<FixedPriorityTaskSchedule*> sched_vect;

      /// @brief Parse the required action.
      ///
      /// This function performs the parsing of the action presented in the
      /// specification file. Additionally, when the system is required to
      /// solve a synthesis problem, the function extracts the parameters of
      /// the synthesis.
      ///
      /// @param optElement is the XML action element.
      void initialParse(tinyxml2::XMLElement *optElement);

      /// @brief Parse a single assignment.
      ///
      /// This function performs the parsing of the assignments presented in the
      /// specification file.
      ///
      /// @param optElement is the XML assignment element.
      void parseAssignment(tinyxml2::XMLElement *assignElement);

      /// @brief Obtain the taskset associated to the assignment.
      ///
      /// This function seeks for the taskset associated to each assignment.
      ///
      /// @param name_taskset is the name of the taskset.
      ///
      /// @return tasksetElement is the XML taskset element.
      tinyxml2::XMLElement *obtainTaskSet(const char *name_taskset);

      /// @brief Parse a single task.
      ///
      /// This function performs the parsing of a single task of the xml 
      /// file.
      ///
      /// @param taskElement is the XML element.
      /// @param reservation is a flag indicating the type of scheduler.
      /// @param Ts is the reservation period for reservation-based schedulers.
      /// @param Qs is the budget for reservation-based schedulers.
      ///
      /// @return the task descriptor of the parsed task.
      GenericTaskDescriptor *parseTask(tinyxml2::XMLElement *taskElement, bool reservation, uint Ts, uint Qs);

      void parseSchedule(tinyxml2::XMLElement *taskElement, const char *name_taskset);

      /// @brief Parse & create the QoS function.
      ///
      /// This function performs the parsing and creation of the QoS function.
      ///
      /// @param taskElement is the XML element.
      std::unique_ptr<QoSFunction> parseQoS(tinyxml2::XMLElement *taskElement);

    public:

      /// @brief Constructor.
      ///
      /// This is the constructor for the parser.
      ///
      /// @param spec_name is the XML specification file.
      /// @param task_name is the XML taskset file.
      Parser(const char *spec_name, const char *task_name) :
          act(NO_ACT),
          opt(NO_OPT),
          optim_eps(1e-6),
          total_bandwidth(1.0),
          verbose(false) {

        /* Create a new XML document */
        taskset = new tinyxml2::XMLDocument();

        /* Check whether the file was loaded */
        if (taskset->LoadFile(task_name) != tinyxml2::XML_SUCCESS) {

          delete taskset;
          taskset = 0;
          EXC_PRINT("ERROR: Cannot open the XML taskset file.");

        }

        /* Create a new XML document */
        spec = new tinyxml2::XMLDocument();

        /* Check whether the file was loaded */
        if (spec->LoadFile(spec_name) != tinyxml2::XML_SUCCESS) {

          delete spec;
          spec = 0;
          EXC_PRINT("ERROR: Cannot open the XML specification file.");

        }

      }

      /// @brief Destructor.
      ///
      /// This is the destructor of the parser.
      ~Parser() {

        /* Delete the specification object */
        if (spec) {
          delete spec;
        }

        /* Delete the taskset object */
        if (taskset) {
          delete taskset;
        }

      }

      /// @brief Return the action.
      ///
      /// This method returns the action to be performed by the solver.
      ///
      /// @return the action to be performed.
      ACTIONS getAction() const {
        return act;
      }

      /// @brief Return the optimization type.
      ///
      /// This method returns the optimization type for the solver.
      ///
      /// @return the used optimization type.
      OPT_TYPE getOptimisationType() const {
        return opt;
      }

      /// @brief Return the optimization epsilon.
      ///
      /// This method returns the optimization epsilon to be used.
      ///
      /// @return the used optimization epsilon.
      double getOptimisationEpsilon() const {
        return optim_eps;
      }

      /// @brief Obtain the total bandwidth.
      ///
      /// This function returns the current total bandwidth.
      ///
      /// @return the current value of the total bandwidth.
      double getTotalBandwidth() const {
        return total_bandwidth;
      }

      /// @brief Set the verbose flag to a specified value.
      ///
      /// This function sets the verbose flag to the value specified as 
      /// parameter.
      ///
      /// @param verbosed is the desidered value for the verbose flag.
      ///
      /// @return the old value of the verbose flag.
      bool setVerbose(bool verbosed) {

        /* Store the old verbose flag */
        bool current = verbose;

        /* Set the new verbose flag */
        verbose = verbosed;

        return current;

      }

      /// @brief Obtain the verbose flag.
      ///
      /// This function returns the current verbose flag.
      ///
      /// @return the current value of the verbose flag.
      bool getVerbose() const {
        return verbose;
      }

      /// @brief Obtain the tasks' vector.
      ///
      /// This function returns the vector of the parsed tasks.
      ///
      /// @return the vector of tasks.
      std::vector<GenericTaskDescriptor*> getTasksVector() {
        return tsk_vect;
      }

      std::vector<FixedPriorityTaskSchedule*> getScheduleVector() {
        return sched_vect;
      }

      /// @brief Parse the XML file.
      ///
      /// This function performs the parsing of the XML file.
      ///
      /// @return the action to be performed.
      ACTIONS parseFile();

  };

}

#endif
