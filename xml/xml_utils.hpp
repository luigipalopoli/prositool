/*!
 * @file    xml_utils.hpp
 * 
 * @brief   This class defines a header for a collection of utility functions.
 * 
 * @author  Luigi Palopoli           <luigi.palopoli@unitn.it>
 *          Bernardo Villalba Frías  <b.r.villalba.frias@hva.nl>
 * 
 * @version 3.0
 * 
 * @date    30 November 2019
 * 
 * This program is a free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation.
 */
#ifndef XML_UTILS_HPP
#define XML_UTILS_HPP

#include "../optimisation/infinity_norm_budget_optimiser.hpp"
#include "xml_parser.hpp"

namespace PrositCore {

  /// @brief Execute the proper probability solver.
  ///
  /// This function executes the corresponding probability solver depending on
  /// the algorithm selected by the user.
  ///
  /// @param v is the vector of the tasks.
  /// @param probability is the vector of the probabilities of deadline hit.
  /// @param quality is the vector of the QoS values.
  /// @param time is the vector of the solution time.
  /// @param p is the pointer to the parser.
  void coreSolveResourceReservations(std::vector<GenericTaskDescriptor*> &v, 
      std::vector<double> &probability, 
      std::vector<double> &quality, 
      std::vector<long long> &time);

  void coreSolveFixedPriority(std::vector<GenericTaskDescriptor*> &v, 
      std::vector<double> &probability, 
      std::vector<double> &quality, 
      std::vector<long long> &time);

  /// @brief Execute the analysis.
  ///
  /// This function executes the analysis for the tasks and presents the 
  /// results.
  ///
  /// @param p is the pointer to the parser.
  void executeSolve(const std::unique_ptr<Parser> &p);

  /// @brief Execute the optimization.
  ///
  /// This function executes the optimization for the tasks and presents the 
  /// results.
  ///
  /// @param p is the pointer to the parser.
  void executeOptimisation(const std::unique_ptr<Parser> &p);

}

#endif
