/*!
 * @file    xml_parser.cpp
 * 
 * @brief   This class defines an implementation for an XML parser for the 
 *          steady state probabilities solver.
 * 
 * @author  Luigi Palopoli           <luigi.palopoli@unitn.it>
 *          Bernardo Villalba Frías  <b.r.villalba.frias@hva.nl>
 * 
 * @version 3.0
 * 
 * @date    30 November 2019
 * 
 * This program is a free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation.
 */
#include "xml_parser.hpp"

using namespace PrositAux;
using namespace tinyxml2;
using namespace Eigen;
using namespace std;

extern bool verbose_flag;
uint deadline = 0;
string path_results;
bool analysis;
string qos_type = "";
double min_target = 0.05;
double max_target = 0.95;
static double eps = 1e-4;
static int max_iteration = 100;

namespace PrositCore {

  /// @brief Parse the XML file.
  ACTIONS Parser::parseFile() {

    /* Look for an optimization tag */
    XMLElement *element = spec->FirstChildElement("synthesis");

    /* Check whether the action is optimization */
    if (element) {

      /* Set the action to OPTIMISE */
      act = OPTIMISE;

      /* Present online information */
      if (verbose_flag) {
        cerr << "Optimisation parsing chosen" << endl;
      }

      /* Update the action flag */
      analysis = false;

    }
    else {

      /* Look for an analysis tag */
      element = spec->FirstChildElement("analysis");

      /* Check whether the action is analysis */
      if (element) {

        /* Set the action to SOLVE */
        act = SOLVE;

        /* Present online information */
        if (verbose_flag) {
          cerr << "Solve parsing chosen" << endl;
        }

        /* Update the action flag */
        analysis = true;

      }
      else {

        /* No actions were found in the XML file */
        EXC_PRINT("ERROR: There is an error in the XML format");

      }

    }

    /* Parse the action */
    initialParse(element);

    return act;

  }

  /// @brief Parse the required action.
  void Parser::initialParse(XMLElement *optElement) {

    if (act == OPTIMISE) {

      const char *method;

      /* Obtain the optimisation method */
      if (!(method = optElement->Attribute("method")))  {
        EXC_PRINT("ERROR: The optimisation method is not defined.");
      }

      /* Check the correctness of the optimisation method */
      if (strcmp(method, "infinity") != 0) {
        EXC_PRINT("ERROR: The optimisation method is not recognized.");
      }

      /* Set the optimisation method to INFINITY_NORM */
      opt = INFINITY_NORM;

      /* Obtain the optimisation epsilon */
      XMLElement *el = optElement->FirstChildElement("epsilon");

      /* Set the value for the optimisation epsilon */
      if (el) {
        el->QueryDoubleText(&optim_eps);
      }
      else {
        optim_eps = 1e-8;
      }

      /* Obtain the total bandwidth */
      el = optElement->FirstChildElement("total_bandwidth");

      /* Set the value for the total bandwidth */
      if (el) {
        el->QueryDoubleText(&total_bandwidth);
      }
      else {
        total_bandwidth = 0.95;
      }

    }

    XMLElement *assignElement = optElement->FirstChildElement("assignment");

    /* There are no <assigment> tags in the file */
    if (!assignElement) {
      EXC_PRINT("ERROR: The assignment section is missing.");
    }

    /* Iterate over the <assignment> tags defined in the XML file */
    while (assignElement) {

      /* Parse the corresponding assignment */
      parseAssignment(assignElement);

      /* Update the assignment */
      assignElement = assignElement->NextSiblingElement();

    }

  }

  /// @brief Parse a single assignment.
  void Parser::parseAssignment(XMLElement *assignElement) {

    /* Variable for the first child parameters of the assignment */
    XMLElement *internal, *scheduler, *tasksetElement;

    const char *sched_type;
    const char *name_taskset;

    /* Obtain the name of the taskset from the assignment */
    if (!(internal = assignElement->FirstChildElement("taskset"))) {
      EXC_PRINT("ERROR: Undefined taskset for assignment");
    }
    name_taskset = internal->GetText();

    /* Obtain the associated taskset */
    tasksetElement = obtainTaskSet(name_taskset);

    /* Obtain the scheduler of the assignment */
    if (!(scheduler = assignElement->FirstChildElement("scheduler"))) {
      EXC_PRINT("ERROR: Undefined scheduler for assignment");
    }

    /* Obtain the type of the scheduler */
    if (!(internal = scheduler->FirstChildElement("type"))) {
      EXC_PRINT("ERROR: Undefined type for the scheduler");
    }
    sched_type = internal->GetText();

    /* Check the correctness of the scheduler type */
    if (strcmp(sched_type, "resource-reservation") == 0) {

      uint Ts, Qs;

      /* Obtain the reservation period */
      if (!(internal = scheduler->FirstChildElement("reservation_period"))) {
        EXC_PRINT("ERROR: Undefined reservation period for the scheduler");
      }
      internal->QueryUnsignedText(&Ts);

      /* Obtain the reservation budget */
      if (!(internal = scheduler->FirstChildElement("budget"))) {

        /* The optimisation process does not use the budget */
        if (!analysis) {
          Qs = Ts;
        }
        else {
          EXC_PRINT("ERROR: Undefined budget for the scheduler");
        }

      }
      else {

        internal->QueryUnsignedText(&Qs);

      }

      /* Look for the tasksets */
      XMLElement *taskElement = tasksetElement->FirstChildElement("task");

      /* There are no <taskset> tags in the file */
      if (!taskElement) {
        EXC_PRINT("ERROR: The task section is missing.");
      }

      /* Iterate over the <tasksets> tags defined in the XML file */
      while (taskElement) {

        /* Add the corresponding task descriptors */
        tsk_vect.push_back(parseTask(taskElement, true, Ts, Qs));

        /* Update the task */
        taskElement = taskElement->NextSiblingElement();

    }

    }
    else {

      if (strcmp(sched_type, "fixed-priority") == 0) {

        /* Parse the required taskset */
        parseSchedule(tasksetElement, name_taskset);

      }
      else {

        EXC_PRINT("ERROR: The scheduler type is not recognized.");

      }

    }

  }

  /// @brief Parse the tasks in the <optimisation> tags.
  XMLElement *Parser::obtainTaskSet(const char *name_taskset) {

    /* Look for the tasksets */
    XMLElement *tasksetElement = taskset->FirstChildElement("taskset");

    /* Variable for the name of the taskset */
    const char *name;

    /* There are no <taskset> tags in the file */
    if (!tasksetElement) {
      EXC_PRINT("ERROR: The taskset section is missing.");
    }

    /* Iterate over the <tasksets> tags defined in the XML file */
    while (tasksetElement) {

      /* Obtain the name of the taskset */
      if (!(name = tasksetElement->Attribute("name"))) {
        EXC_PRINT("ERROR: Undefined name for the taskset");
      }

      /* The taskset is associated with the assignment */
      if (strcmp(name, name_taskset) == 0) {

        /* Obtain the associated taskset */
        return tasksetElement;

      }

      /* Update the taskset */
      tasksetElement = tasksetElement->NextSiblingElement();

    }

    /* The required taskset does not exists */
    EXC_PRINT("ERROR: The required taskset is missing.");

  }

  /// @brief Parse a single task.
  GenericTaskDescriptor *Parser::parseTask(XMLElement *taskElement, bool reservation, uint Ts, uint Qs) {

    GenericTaskDescriptor *task_des = nullptr;

    /* Variables for the attributes of the task */
    const char *name;
    const char *type;
    const char *algorithm;
    uint T = 0;
    uint granularity = 1;
    uint num_modes = 1;

    /* Variables for the inter-arrival distribution range */
    uint64_t zmin = numeric_limits<uint64_t>::max();
    uint64_t zmax = numeric_limits<uint64_t>::min();
    const char *path_inter_time = "";

    /* Vector for the computation times */
    std::vector<std::unique_ptr<pmf>> comp_times;

    /* Variable for the first childs parameters of the task */
    XMLElement *internal;

    /* Obtain the name of the task */
    if (!(name = taskElement->Attribute("name"))) {
      EXC_PRINT("ERROR: Undefined name for the task");
    }

    /* Obtain the type of the task */
    if (!(type = taskElement->Attribute("type"))) {
      EXC_PRINT_2("ERROR: Undefined type for task ", name);
    }

    /* Check the correctness of the task type */
    if (analysis) {

      /* Resource reservation */
      if (reservation) {

        if ((strcmp(type, "periodic") != 0) && (strcmp(type, "aperiodic") != 0)) {
          EXC_PRINT("ERROR: The task type is not recognized.");
        }

      }
      else {

        if (strcmp(type, "periodic") != 0) {
          EXC_PRINT("ERROR: The task type is not recognized.");
        }

      }

    }
    else {

      /* Check the correctness of the task type */
      if (strcmp(type, "periodic") != 0) {
        EXC_PRINT("ERROR: The task type is not recognized.");
      }

    }

    /* Obtain the solver algorithm of the task */
    if (!(algorithm = taskElement->Attribute("algorithm"))) {
      algorithm = "cyclic";
    }

    /* The Cyclic-Reduction algorithm is set as default */
    if ((strcmp(algorithm, "cyclic") != 0) && 
        (strcmp(algorithm, "logarithmic") != 0) &&
        (strcmp(algorithm, "analytic") != 0) &&
        (strcmp(algorithm, "companion") != 0)) {

      algorithm = "cyclic";

    }

    /* Obtain the task period */
    if ((internal = taskElement->FirstChildElement("period"))) {
      internal->QueryUnsignedText(&T);
    }

    /* Obtain the sampling granularity */
    if ((internal = taskElement->FirstChildElement("granularity"))) {
      internal->QueryUnsignedText(&granularity);
    }

    /* Obtain the number of modes */
    if ((internal = taskElement->FirstChildElement("num_modes"))) {
      internal->QueryUnsignedText(&num_modes);
    }

    /* Iterate over the number of modes */
    for (uint64_t i = 1; i <= num_modes; i++) {

      /* Variables for the distributions ranges */
      uint64_t cmin = numeric_limits<uint64_t>::max();
      uint64_t cmax = numeric_limits<uint64_t>::min();

      /* Variable for the name of the computation times */
      char comp_time_tag[250];
      const char *path_comp_time;

      /* Build the string with the name */
      sprintf(comp_time_tag, "computation_time%lu", i);

      /* Obtain the path to the computation time */
      if (!(internal = taskElement->FirstChildElement(comp_time_tag))) {
        EXC_PRINT_2("ERROR: Undefined computation time path for task", name);
      }
      path_comp_time = internal->GetText();

      /* Check the PMF of the computation time */
      obtainRange(path_comp_time, cmin, cmax);

      /* Variable for the PMF of the computation time */
      unique_ptr<pmf> c(new pmf(cmin, cmax, 1e-4));

      /* Load the PMF of the computation time */
      c->load(path_comp_time);

      /* Push the computation times in the vector */
      comp_times.push_back(move(c));

    }

    /* Obtain the path to the interarrival time */
    if ((internal = taskElement->FirstChildElement("interarrival_time"))) {

      path_inter_time = internal->GetText();

      /* Check the PMF of the interarrival time */
      obtainRange(path_inter_time, zmin, zmax);

    }
    else {

      /* The task is periodic */
      if (T != 0) {

        /* Minimum and maximum indices equal to the task period */
        zmin = T;
        zmax = T;

      }
      else {
        EXC_PRINT("ERROR: The distribution of the interarrival time is missing.");
      }

    }

    /* Check the periodicity of the fixed-priority scheduler */
    if ((zmin != zmax) && (!reservation)) {

      EXC_PRINT("ERROR: The fixed-priority analysis is performed only over periodic tasks.");

    }

    /* Variable for the PMF of the interarrival time */
    unique_ptr<pmf> u(new pmf(zmin, zmax, 1e-4));

    /* The PMF for the interarrival time is passed */
    if (internal) {

      /* Load the PMF of the interarrival time */
      u->load(path_inter_time);

    }
    else {

      /* Set the PMF of the interarrival time */
      u->set(0, 1.0);

    }

    /* Obtain the relative deadline */
    if (!(internal = taskElement->FirstChildElement("deadline"))) {

      if (T != 0) {
        deadline = T;
      }
      else {
        deadline = zmin;
      }

    }
    else {
      internal->QueryUnsignedText(&deadline);
    }

    /* Check the maximum deadline to analyse */
    if (deadline <= 0) {
      EXC_PRINT("ERROR: The maximum deadline has not been properly set.");
    }

    if (reservation) {

      const char *path_trans_mat;

      /* Variable for the transition matrix */
      MatrixXd transition_matrix = MatrixXd::Zero(num_modes, num_modes);
    
      /* Load the transition matrix */
      if (num_modes == 1) {
        transition_matrix(0, 0) = 1;
      }
      else {

        /* The probability transition matrix is passed */
        if ((internal = taskElement->FirstChildElement("transition_matrix"))) {

          path_trans_mat = internal->GetText();
          loadMatrix(path_trans_mat, transition_matrix);

        }
        else {
          EXC_PRINT("ERROR: The probability transition matrix is missing.");
        }

      }

      /* Present online information */
      if (verbose_flag) {
        cerr << "A task scheduled by a resource reservation algorithm has been defined in the XML file" << endl;
      }

      /* Create the task descriptor */
      task_des = new ResourceReservationTaskDescriptor(name, num_modes, move(comp_times), move(u->resample(Ts, false)), transition_matrix, Qs, Ts, granularity, deadline, algorithm);

      /* Set the parameters for the task descriptor */
      task_des->setDeadlineStep(Ts);

      /* Present online information */
      if (verbose_flag) {
        cerr << "A resource reservation task descriptor object was created successfully!" << endl;
      }

      /* Set the analytic solver */
      if (strcmp(algorithm, "analytic") == 0) {

        if (task_des->getNumModes() == 1) {

          unique_ptr<ResourceReservationProbabilitySolver> ps(new AnalyticResourceReservationProbabilitySolver());
          task_des->setSolver(move(ps));

        }
        else {

          EXC_PRINT("ERROR: The analytic solver is only implemented for single mode tasks.");

        }

      }
      /* Set the companion solver */
      else if (strcmp(algorithm, "companion") == 0) {

        if (task_des->getNumModes() == 1) {

          unique_ptr<ResourceReservationProbabilitySolver> ps(new CompanionResourceReservationProbabilitySolver(/*max_deadline*/));
          task_des->setSolver(move(ps));

        }
        else {

          EXC_PRINT("ERROR: The companion solver is only implemented for single mode tasks.");

        }

      }
      /* Set the cyclic reduction solver */
      else if (strcmp(algorithm, "cyclic") == 0) {

        unique_ptr<QBDResourceReservationProbabilitySolver> ps(new CRQBDResourceReservationProbabilitySolver(false, max_iteration));
        task_des->setSolver(move(ps));

      }
      /* Set the logarithmic reduction solver */
      else if (strcmp(algorithm, "logarithmic") == 0) {

        unique_ptr<QBDResourceReservationProbabilitySolver> ps(new LatoucheQBDResourceReservationProbabilitySolver(eps, max_iteration));
        task_des->setSolver(move(ps));

      }
      else {

        EXC_PRINT("ERROR: The solver is not recognized.");

      }

    }
    else {

      uint offset = 0;
      uint priority;

      /* Obtain the initial phase */
      if ((internal = taskElement->FirstChildElement("offset"))) {
        internal->QueryUnsignedText(&offset);
      }

      /* Obtain the task priority */
      if (!(internal = taskElement->FirstChildElement("priority"))) {
        EXC_PRINT_2("ERROR: Undefined priority for task", name);
      }
      internal->QueryUnsignedText(&priority);

      /* Present online information */
      if (verbose_flag) {
        cerr << "A task scheduled by a fixed-priority algorithm has been defined in the XML file" << endl;
      }

      /* Create the task descriptor */
      task_des = new FixedPriorityTaskDescriptor(name, move(comp_times[0]->resample(granularity, true)), move(u), priority, deadline, offset);

      /* Set the parameters for the task descriptor */
      task_des->setDeadlineStep(deadline);

      /* Present online information */
      if (verbose_flag) {
        cerr << "A fixed priority task descriptor object was created successfully!" << endl;
      }

    }

    /* Insert the deadline in the probability map */
    for (uint64_t i = 1; i <= ceil(deadline / Ts); i++) {
      task_des->insertDeadline(task_des->getDeadlineStep() * i);
    }

    /* Set the parameters for the task descriptor */
    task_des->setVerboseFlag(verbose_flag ? true : false);

    /* Parse and create the QoS function for the task */
    task_des->q = move(parseQoS(taskElement));

    /* Set the type of the QoS function */
    task_des->qos_type = qos_type;

    /* Set the QoS targets */
    task_des->setMinQoSTarget(min_target);
    task_des->setMaxQoSTarget(max_target);

    return task_des;

  }

  /// @brief Parse a single schedule.
  void Parser::parseSchedule(XMLElement *tasksetElement, const char *name_taskset) {

    FixedPriorityTaskSet taskset;

    /* Obtain the tasks */
    XMLElement *taskElement = tasksetElement->FirstChildElement("task");

    /* There are no <task> tags in the file */
    if (!taskElement) {
      EXC_PRINT("ERROR: The task section is missing.");
    }

    /* Iterate over the <task> tags defined in the XML file */
    while (taskElement) {

      GenericTaskDescriptor *td = parseTask(taskElement, false, 1, 0);

      FixedPriorityTaskDescriptor *fptd;

      if (!(fptd = dynamic_cast<FixedPriorityTaskDescriptor *>(td))) {
        EXC_PRINT_2("Impossible to cast task GenericTaskDescriptor to ResourceReservationTaskDescriptor for task ", td->getName());
      }

      /* Add the corresponding task descriptors */
      taskset.emplace_back(fptd);

      /* Update the task */
      taskElement = taskElement->NextSiblingElement();

    }

    FixedPriorityTaskSchedule *task_sched = new FixedPriorityTaskSchedule(name_taskset, move(taskset));

    /* Set the parameters for the task schedule */
    task_sched->setVerboseFlag(verbose_flag);

    unique_ptr<FixedPriorityProbabilitySolver> ps(new FixedPriorityProbabilitySolver(eps, max_iteration));
    task_sched->setSolver(move(ps));

    sched_vect.push_back(task_sched);

  }

  /// @brief Parse & create the QoS function.
  unique_ptr<QoSFunction> Parser::parseQoS(XMLElement *taskElement) {

    XMLElement *qosElement;
    XMLElement *internal;

    /* Initialize the type of the QoS function */
    qos_type = "";

    /* If exists, all the parameters have to be set */
    if ((qosElement = taskElement->FirstChildElement("qosfun"))) {

      const char *type;
      double scale = 0.0, qos_min = 0.0, qos_max = 0.0, offset = 0.0;

      /* Obtain the type of the QoS function */
      if (!(type = qosElement->Attribute("type"))) {
        EXC_PRINT("ERROR: Undefined type for the QoS function.");
      }

      /* Check the correct tye of the QoS function */
      if ((strcmp(type, "linear") == 0) || (strcmp(type, "quadratic") == 0)) {

        /* Set the type of the QoS function */
        qos_type = type;

      }
      else {

        EXC_PRINT("ERROR: The QoS function type is not recognized.");

      }

      /* Obtain the minimum of the QoS function */
      if (!(internal = qosElement->FirstChildElement("pmin"))) {
        EXC_PRINT("ERROR: Minimum not specified.");
      }
      internal->QueryDoubleText(&qos_min);

      /* Obtain the maximum of the QoS function */
      if (!(internal = qosElement->FirstChildElement("pmax"))) {
        EXC_PRINT("ERROR: Maximum not specified.");
      }
      internal->QueryDoubleText(&qos_max);

      /* Obtain the scaling factor of the QoS function */
      if (!(internal = qosElement->FirstChildElement("scale"))) {
        EXC_PRINT("ERROR: Scaling factor not specified.");
      }
      internal->QueryDoubleText(&scale);

      /* Obtain the offset of the QoS function */
      if ((internal = qosElement->FirstChildElement("offset"))) {

        internal->QueryDoubleText(&offset);

      }

      /* Check the QoS targets for the optimisation */
      if ((internal = qosElement->FirstChildElement("qos_max_target"))) {

        /* Obtain the maximum target */
        internal->QueryDoubleText(&max_target);

      }

      /* Check whether the minimum target is defined */
      /* Obtain the reservation period */
      if (!(internal = qosElement->FirstChildElement("qos_min_target"))) {
        EXC_PRINT("ERROR: Undefined minimum QoS target");
      }
      internal->QueryDoubleText(&min_target);

      /* Present online information */
      if (verbose_flag) {
        cerr << "QoS function parameters: " << endl;
        cerr << "  QoS type:     " << type << endl;
        cerr << "  Scale factor: " << scale << endl;
        cerr << "  Minimum:      " << qos_min << endl;
        cerr << "  Maximum:      " << qos_max << endl;
        cerr << "  Offset:       " << offset << endl;
        cerr << "  Min target:   " << min_target << endl;
        cerr << "  Max_target:   " << max_target << endl;
      }

      /* Check whether the QoS parameters are present */
      if ((scale + qos_min + qos_max) != 0.0) {

        unique_ptr<QoSFunction> u;

        /* Create the QoS function */
        if (qos_type == "linear") {
          unique_ptr<QoSFunction> t(new LinearQoSFunction(scale, qos_min, qos_max, offset));
          u = move(t);
        }
        else if (qos_type == "quadratic") {
          unique_ptr<QoSFunction> t(new QuadraticQoSFunction(scale, qos_min, qos_max));
          u = move(t);
        }

        return u;

      }

    }
    else {

      /* Optimisation tag */
      if (!analysis) {

        EXC_PRINT("ERROR: The QoS parameters are mandatory in the optimisation case.");

      }
      else {

        if (verbose_flag) {
          cerr << "The QoS parameters are not present in the XML file." << endl;
        }

      }

    }

    return nullptr;

  }

}
