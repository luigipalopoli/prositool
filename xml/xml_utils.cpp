/*!
 * @file    xml_utils.cpp
 * 
 * @brief   This class defines an implementation for a collection of utility
 *          functions.
 * 
 * @author  Luigi Palopoli           <luigi.palopoli@unitn.it>
 *          Bernardo Villalba Frías  <b.r.villalba.frias@hva.nl>
 * 
 * @version 3.0
 * 
 * @date    30 November 2019
 * 
 * This program is a free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation.
 */
#include "xml_utils.hpp"

using namespace PrositAux;
using namespace std;

extern bool verbose_flag;
extern uint deadline;
extern string path_results;
extern long long t_start;
extern long long t_xml_parse_end;

namespace PrositCore {

  /// @brief Execute the proper probability solver.
  void coreSolveFixedPriority(FixedPriorityTaskSchedule *s, vector<double> &probability, vector<double> &quality, vector<long long> &time) {

    /* Variables for the time analysis */
    long long t_solution_start_i = 0, t_solution_end_i = 0;

    /* Obtain the initial computation time */
    t_solution_start_i = my_get_time();

    /* Compute the probability of respecting the deadline */
    s->computeProbability();

    /* Obtain the final computation time */
    t_solution_end_i = my_get_time();

    /* Obtain the number of tasks in the taskset */
    size_t num_tasks = probability.size();

    /* Obtain the average of the solution time */
    long long t_solution = (t_solution_end_i - t_solution_start_i) / num_tasks;

    /* Iterate over the tasks in the taskset */
    for (size_t i = 0; i < num_tasks; i++) {

      /* Obtain the initial time */
      t_solution_start_i = my_get_time();

      /* Obtain the probability */
      probability[i] = s->getProbability(((s->getTaskSet())[i])->getDeadlineStep(), i);

      /* Obtain the final time */
      t_solution_end_i = my_get_time();

      quality[i] = 0.0;

      /* Obtain the computation time */
      time[i] = t_solution + (t_solution_end_i - t_solution_start_i);

    }

  }

  /// @brief Execute the proper probability solver.
  void coreSolveResourceReservations(vector<GenericTaskDescriptor*> &v, vector<double> &probability, vector<double> &quality, vector<long long> &time) {

    /* Variables for the time analysis */
    long long t_solution_start_i = 0, t_solution_end_i = 0;

    int i = 0;

    /* Iterate over the tasks in the vector */
    for (vector<GenericTaskDescriptor*>::iterator it = v.begin(); it != v.end(); it++) {

      /* Obtain the initial computation time */
      t_solution_start_i = my_get_time();

      ResourceReservationTaskDescriptor *t;

      if (!(t = dynamic_cast<ResourceReservationTaskDescriptor *>((*it)))) {
        EXC_PRINT_2("Impossible to cast task GenericTaskDescriptor to ResourceReservationTaskDescriptor for task ", (*it)->getName());
      }

      /* Reset the solver to ensure the new calculation of the probability */
      t->resetSolver();

      /* Obtain the probability */
      probability[i] = t->getProbability(t->getDeadline());

      /* Check whether the QoS function exists */
      if (!(t->q)) {

        /* No QoS function */
        quality[i] = 0.0;

      }
      else {

        /* Evaluate the QoS function */
        quality[i] = t->q->evaluateQuality(probability[i]);

      }

      /* Obtain the final computation time */
      t_solution_end_i = my_get_time();

      /* Obtain the computation time */
      time[i] = t_solution_end_i - t_solution_start_i;

      /* Update the index of the tasks */
      i++;

    }

  }

  /// @brief Execute the analysis.
  void executeSolve(const unique_ptr<Parser> &p) {

    /* Variables for the time analysis */
    long long t_solution_start = 0, t_solution_end = 0;

    /* Obtain the initial computation time */
    t_solution_start = my_get_time();

    /* Obtain the total number of reservation based tasks */
    size_t num_rr_tasks = (p->getTasksVector()).size();

    /* Obtain the total number of reservation based tasks */
    size_t num_fp_sets = (p->getScheduleVector()).size();

    if (num_rr_tasks > 0) {

      /* Obtain the vector with the tasks */
      vector<GenericTaskDescriptor*> v = p->getTasksVector();

      /* Variables for the results */
      vector<double> probability(num_rr_tasks);
      vector<double> quality(num_rr_tasks);
      vector<long long> time(num_rr_tasks);

      /* Variable for the log files */
      ofstream results_file;

      /* Apply the solver to the tasks */
      coreSolveResourceReservations(v, probability, quality, time);

      /* Obtain the final computation time */
      t_solution_end = my_get_time();

      /* Present the header for the results */
      cout << "=====================================================================================================================" << endl;
      cout << "=                                                      Results                                                      =" << endl;
      cout << "=====================================================================================================================" << endl;
      cout << setw(8) << "Name" 
           << setw(23) << "Assigned Budget" 
           << setw(17) << "Bandwidth" 
           << setw(27) << "Probability" 
           << setw(20) << "Quality" 
           << setw(15) << "Time" << endl;

      /* Variables for the iterations */
      int i = 0;
      double total_bandwidth = 0.0;
      vector<GenericTaskDescriptor*>::iterator it;

      /* The name of the result file is passed as argument */
      if (!path_results.empty()) {

        /* Open the log file */
        results_file.open(path_results);

        /* Set the initial value to zero */
        results_file << "0 0.00000000000000000000" << endl;

      }

      /* Iterate over the vector of tasks */
      for (it = v.begin(); it != v.end(); it++) {

        ResourceReservationTaskDescriptor *t;

        if (!(t = dynamic_cast<ResourceReservationTaskDescriptor *>((*it)))) {
          EXC_PRINT_2("Impossible to cast task GenericTaskDescriptor to ResourceReservationTaskDescriptor for task ", (*it)->getName());
        }

        /* Present the individual results */
        t->display("", t, probability, quality, time, i);

        /* The name of the result file is passed as argument */
        if (!path_results.empty()) {

          //results_file << fixed << task_des.getDeadlineStep() * i << " "
                       //<< setprecision(20) << probability << endl;

        }

        /* Increase the total used bandwdidth */
        total_bandwidth += t->bandwidth;

        /* Update the index */
        i++;

      }

      /* The name of the result file is passed as argument */
      if (!path_results.empty()) {

        /* Close the log file */
        results_file.close();

      }

      /* Obtain the minimum infinite norm */
      double inf_norm_final = *min_element(quality.begin(), quality.end());

      /* Present the final results */
      cout << "=====================================================================================================================" << endl;
      cout << setw(20) << "Total bandwidth:"     << setw(15) << total_bandwidth << endl;
      cout << setw(18) << "Infinity norm:" << setw(17) << inf_norm_final  << endl;
      cout << "=====================================================================================================================" << endl;
      cout << "=                                                 Computation  Time                                                 =" << endl;
      cout << "=====================================================================================================================" << endl;
      cout << setw(17) << "Parsing time:"  << setw(18) << t_xml_parse_end - t_start         << " us" << endl;
      cout << setw(18) << "Solution time:" << setw(17) << t_solution_end - t_solution_start << " us" << endl;
      cout << setw(15) << "Total time:"    << setw(20) << t_solution_end - t_start          << " us" << endl;
      cout << "=====================================================================================================================" << endl;

    }

    if (num_fp_sets > 0) {

      /* Obtain the vector with the tasks */
      vector<FixedPriorityTaskSchedule*> s = p->getScheduleVector();

      double inf_norm_final = 0.0;

      /* Present the header for the results */
      cout << "=====================================================================================================================" << endl;
      cout << "=                                                      Results                                                      =" << endl;
      cout << "=====================================================================================================================" << endl;
      cout << setw(11) << "TaskSet" 
           << setw(9) << "Task" 
           << setw(13) << "Priority" 
           << setw(11) << "Period" 
           << setw(14) << "Deadline" 
           << setw(22) << "Probability" 
           << setw(17) << "Quality" 
           << setw(13) << "Time" << endl;

      for (size_t i = 0; i < num_fp_sets; i++) {

        size_t num = (s[i]->getTaskSet()).size();

        /* Variables for the results */
        vector<double> probability(num);
        vector<double> quality(num);
        vector<long long> time(num);

        /* Apply the solver to the tasksets */
        coreSolveFixedPriority(s[i], probability, quality, time);

        /* Variables for the iterations */
        int j = 0;

        /* Obtain the vector with the tasks */
        vector<FixedPriorityTaskDescriptor*> v = s[i]->getTaskSet();
        vector<FixedPriorityTaskDescriptor*>::iterator it;

        /* Iterate over the tasks in the taskset */
        for (it = v.begin(); it != v.end(); it++) {

          /* Present the individual results */
          (*it)->display(s[i]->getName(), (*it), probability, quality, time, j);

          /* Update the index */
          j++;

        }

        /* Obtain the final computation time */
        t_solution_end = my_get_time();

        /* Obtain the minimum infinite norm */
        inf_norm_final = *min_element(quality.begin(), quality.end());

      }

      /* Present the final results */
      cout << "=====================================================================================================================" << endl;
      cout << setw(18) << "Infinity norm:" << setw(17) << inf_norm_final  << endl;
      cout << "=====================================================================================================================" << endl;
      cout << "=                                                 Computation  Time                                                 =" << endl;
      cout << "=====================================================================================================================" << endl;
      cout << setw(17) << "Parsing time:"  << setw(18) << t_xml_parse_end - t_start         << " us" << endl;
      cout << setw(18) << "Solution time:" << setw(17) << t_solution_end - t_solution_start << " us" << endl;
      cout << setw(15) << "Total time:"    << setw(20) << t_solution_end - t_start          << " us" << endl;
      cout << "=====================================================================================================================" << endl;

    }

  }

  /// @brief Execute the optimization.
  void executeOptimisation(const unique_ptr<Parser> &p) {

    /* Variables for the time analysis */
    long long t_optimisation_start = 0, t_optimisation_set_up = 0, t_optimisation_end = 0;

    /* Obtain the initial computation time */
    t_optimisation_set_up = my_get_time();

    /* Obtain the vector with the tasks */
    vector<GenericTaskDescriptor*> v = p->getTasksVector();

    /* Obtain the total number of tasks */
    int num = v.size();

    /* Declare the optimiser */
    InfinityNormBudgetOptimiser Optimiser(v, p->getOptimisationEpsilon(), p->getTotalBandwidth());

    /* Initialize the bound for the optimization */
    Optimiser.initTargetBounds();

    /* Set the verbose flag */
    if (verbose_flag) {
      Optimiser.setVerboseFlag(true);
    }

    /* Obtain the initial optimisation time */
    t_optimisation_start = my_get_time();

    /* Perform the optimisation */
    Optimiser.optimise();

    /* Obtain the final optimisation time */
    t_optimisation_end = my_get_time();
  
    /* Check the success of the optimisation */
    if (Optimiser.getState() != GenericBudgetOptimiser::OK) {

      cerr << "Optimisation failed" << endl;
      return;

    }

    /* Variables for the results */
    vector<double> probability(num);
    vector<double> quality(num);
    vector<long long> time(num);

    /* Apply the solver to the tasks */
    coreSolveResourceReservations(v, probability, quality, time);

    /* Obtain the final computation time */
    t_optimisation_end = my_get_time();

    /* Present the header for the results */
    cout << "=====================================================================================================================" << endl;
    cout << "=                                                      Results                                                      =" << endl;
    cout << "=====================================================================================================================" << endl;
    cout << setw(8) << "Name" 
         << setw(23) << "Computed Budget" 
         << setw(17) << "Bandwidth" 
         << setw(27) << "Probability" 
         << setw(23) << "Quality" 
         << setw(12) << "Time" << endl;

    /* Variables for the iterations */
    int i = 0;
    double total_bandwidth = 0.0;
    vector<GenericTaskDescriptor*>::iterator it;

    /* Iterate over the vector of tasks */
    for (it = v.begin(); it != v.end(); it++) {

      ResourceReservationTaskDescriptor *t;

      if (!(t = dynamic_cast<ResourceReservationTaskDescriptor *>((*it)))) {
        EXC_PRINT_2("Impossible to cast task GenericTaskDescriptor to ResourceReservationTaskDescriptor for task ", (*it)->getName());
      }

      /* Present the individual results */
      t->display("", t, probability, quality, time, i);

      /* Increase the total used bandwdidth */
      total_bandwidth += t->bandwidth;

      /* Update the index */
      i++;

    }

    /* Obtain the minimum infinite norm */
    double inf_norm_final = *min_element(quality.begin(), quality.end());

    /* Present the final results */
    cout << "=====================================================================================================================" << endl;
    cout << setw(20) << "Total bandwidth:"     << setw(15) << total_bandwidth << endl;
    cout << setw(18) << "Infinity norm:" << setw(17) << inf_norm_final  << endl;
    cout << "=====================================================================================================================" << endl;
    cout << "=                                                 Computation  Time                                                 =" << endl;
    cout << "=====================================================================================================================" << endl;
    cout << setw(17) << "Parsing time:"       << setw(23) << t_xml_parse_end - t_start                    << " us" << endl;
    cout << setw(23) << "Optimisation setup:" << setw(17) << t_optimisation_start - t_optimisation_set_up << " us" << endl;
    cout << setw(22) << "Optimisation time:"  << setw(18) << t_optimisation_end - t_optimisation_start    << " us" << endl;
    cout << setw(15) << "Total time:"         << setw(25) << t_optimisation_end - t_start                 << " us" << endl;
    cout << "=====================================================================================================================" << endl;

  }

}
