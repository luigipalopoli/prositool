# The PROSIT tool #

PROSIT is a software tool, which supports design and analysis of real–time
systems based on the notion of probabilistic deadlines. It supports both
fixed–priority and CPU reservations scheduling. Specifically, for
fixed–priority scheduling, the tool allows modelling a set of periodic tasks,
each having a random computation time described by a probability distribution.

For CPU reservations, the tool supports both periodic and aperiodic tasks: each
task is described by a random computation time and (possibly) inter–arrival
time. Notably, the computation time can be either an i.i.d. or a Markov
Modulated Process. For periodic tasks scheduled by the CPU reservations, the
tool features solve a design problem, namely, an optimisation algorithm for the
choice of scheduling parameters that maximise a global Quality of Service
function.

### Preparation ###

The PROSIT tool is a set of applications and libraries developed in C++,
released under the GNU GPL license and designed to be flexible and extensible.
It consists of a probabilistic analysis tool, a budget optimiser for the
probabilistic analysis and an estimator of the parameters of the Markov
Computation Time Model (MCTM). In order to perform all the vector and matrix
calculations, PROSIT relies on the Eigen library. Therefore, in order to
compile and use the tool, it is necessary to check the availability of the
required dependencies. Hence, open a Terminal and introduce the following
commands:

~$ sudo apt-get update

~$ sudo apt-get install build-essential libeigen3-dev libeigen3-doc

~$ gcc --version

gcc (Ubuntu 4.8.4-2ubuntu1~14.04.3) 4.8.4

Copyright (C) 2013 Free Software Foundation, Inc.

This is free software; see the source for copying conditions.  There is NO

warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Download ###

Now that the dependencies are in order, it is possible to download and compile
the PROSIT tool. Open a Terminal and introduce the following commands:

~$ git clone https://bitbucket.org/luigipalopoli/prositool.git PROSIT

~$ cd PROSIT

~/PROSIT$ make -j $(nproc)

The development of the PROSIT tool has been performed exclusively on a Linux
environment, however it is assumed that it will compile and execute in a MacOS
environment without any further modification.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
